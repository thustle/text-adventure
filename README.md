# Text Adventure!

This application consists of the following:
1. A text adventure game engine, which can be used to run any compatible game file (the game/save files are just JSON). The engine can be used by a different UIs (basic text, GUI, web, etc.) to create a game.
2. A relatively simple example text adventure game file to show the features of the engine.
3. A terminal UI game (using Bubble Tea) which uses the engine and the provided example game file. It features animated visual indications of health, the player's inventory and the navigation options. There are also shortcuts to help with interacting with the game (see the help screen in the game).

You can quickly run the game with the command 'go ./app/cmd'
Alternatively, use the Makefile to build, run and test the game.

The engine part of the app is pretty well unit tested, but the bubble tea 
interface isn't much (other than an attempt to use the github.com/charmbracelet/x/exp/teatest
package to check the generated view of the help screen). I haven't come across 
any obvious bugs, though.

## Screenshots
Demo
![Game screen layout (iTerm2)](readme/demo.gif)

Game on iTerm2
![Game screen layout (iTerm2)](readme/screenshot1.png)

Help screen on iTerm2
![Help screen layout (iTerm2)](readme/screenshot2.png)

Terminal.app
![Game screen layout (Mac Terminal.app)](readme/screenshot3.png)

## Milestones
- [x] Basic Layout
- [X] Engine feedback to TUI
- [X] Basic Engine (look, move, eat, pick, drop, wield)
- [X] Combat mechanics
- [X] More advanced commands (use items with others, unlock new directions)
- [X] Complete in-built example game
- [X] Finish creating Game data
- [X] Make game data loadable and savable
- [X] Save/Load screens in UI
- [X] Tidy up code and add more tests

## License
[MIT](LICENSE)
