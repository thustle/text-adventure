package main

import (
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/x/exp/teatest"
	"github.com/muesli/termenv"
	"io"
	"testing"
	"time"
)

func init() {
	lipgloss.SetColorProfile(termenv.Ascii)
}

func TestHelpOutput(t *testing.T) {
	help := newHelp(80, 25, NewStyles(lipgloss.DefaultRenderer()), nil)
	tm := teatest.NewTestModel(t, help, teatest.WithInitialTermSize(80, 25))
	time.Sleep(100 * time.Millisecond)
	out, err := io.ReadAll(tm.Output())
	if err != nil {
		t.Error(err)
	}
	teatest.RequireEqualOutput(t, out)
}
