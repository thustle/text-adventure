package main

import (
	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"strings"
)

const shortcutContent = `Shift+cursor keys = Go North/South/East/West
Ctrl+c = Quit the game
Ctrl+j / Ctrl+k = Page through your inventory
Cursor Up / Down = Scroll through command history`

const commandsContent = `quit / clear = Quit the game / Clear the text area
go east = Move East
look = Describes your surroundings
look at stick = Look at an inventory item or room item
pick up stick / get stick = Pick up the item
drop stick = Drop the item
attack troll / hit troll = Attack the specified enemy
wield stick = Use the stick as your weapon
eat apple / drink potion = Consume the item
use x with y / use x on y = Use an item on/with another
talk to wizard = Talk to the character
give x to elf / offer x to elf = Give item to character`

func createCommandRows() []table.Row {
	lines := strings.Split(commandsContent, "\n")
	rows := make([]table.Row, len(lines))
	for i, line := range lines {
		cols := strings.Split(line, " = ")
		rows[i] = table.Row{cols[0], cols[1]}
	}
	return rows
}

type help struct {
	width         int
	height        int
	styles        *Styles
	returnModel   tea.Model
	commandsTable table.Model
}

func newHelp(width, height int, styles *Styles, returnModel tea.Model) help {
	columns := []table.Column{
		{Title: "Command", Width: (width / 2) - 13},
		{Title: "Description", Width: (width / 2) + 7},
	}
	commandsTable := table.New(
		table.WithColumns(columns),
		table.WithRows(createCommandRows()),
		table.WithFocused(false),
		table.WithHeight(height-12),
	)
	s := table.DefaultStyles()
	s.Header = s.Header.
		BorderStyle(lipgloss.HiddenBorder()).
		Background(lipgloss.Color("62")).
		Foreground(lipgloss.Color("230")).
		BorderBottom(false).
		Bold(false)
	s.Selected = s.Selected.
		Foreground(lipgloss.Color("7")).
		Background(lipgloss.Color("0")).
		Bold(false).
		Margin(0, 0, 0, 0).
		Padding(0, 0, 0, 0)
	commandsTable.SetStyles(s)

	return help{
		width:         width,
		height:        height,
		styles:        styles,
		returnModel:   returnModel,
		commandsTable: commandsTable,
	}
}

// Init is run once when the program starts
func (m help) Init() tea.Cmd {
	return nil
}

// Update handles game state changes
func (m help) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
		m.commandsTable.SetHeight(m.height - 12)
		m.commandsTable.SetWidth(m.width - 2)
	case tea.KeyMsg:
		switch msg.String() {
		case "esc":
			return m.returnModel, nil
		}
	}
	return m, nil
}

func (m help) View() string {
	title := "Help"
	var titleStyle = lipgloss.NewStyle().
		Background(lipgloss.Color("62")).
		Foreground(lipgloss.Color("230")).
		Padding(0, 2).
		MarginLeft((m.width / 2) - len(title) - 1)
	m.commandsTable.SetWidth(m.width - 2)

	header := m.appBoundaryView("Text Adventure Help Page")
	body := borderStyle.Width(m.width - 2).Height(m.height - 4).
		Render(
			titleStyle.Render(title) + "\n" +
				headingStyle.Render("Shortcuts") + "\n" +
				shortcutContent + "\n\n" +
				headingStyle.Render("Example Commands") + "\n" +
				m.commandsTable.View())

	footer := m.appBoundaryView("<ESC> to go back")

	return m.styles.Base.Render(header + "\n" + body + "\n" + footer)
}

func (m help) appBoundaryView(text string) string {
	return lipgloss.PlaceHorizontal(m.width, lipgloss.Left, m.styles.HeaderText.Render("+-- "+text), lipgloss.WithWhitespaceChars("/"), lipgloss.WithWhitespaceForeground(indigo))
}
