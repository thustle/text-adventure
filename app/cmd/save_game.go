package main

import (
	"fmt"
	"github.com/charmbracelet/bubbles/filepicker"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"os"
	"strings"
	"text-adventure/pkg/engine"
	"time"
)

type saveGame struct {
	game        *engine.Game
	width       int
	height      int
	styles      *Styles
	returnModel tea.Model
	dir         string
	err         error
	gameName    string
	message     string
	inputField  textinput.Model
	filePicker  filepicker.Model
}

func newSaveGame(game *engine.Game, width, height int, styles *Styles, returnModel tea.Model, gameName string) saveGame {
	dir, err := getAppDataPath()
	inputField := textinput.New()
	inputField.Focus()
	inputField.Reset()
	filePicker := filepicker.New()
	filePicker.AllowedTypes = []string{".sav", ".json"}
	filePicker.CurrentDirectory = dir
	filePicker.ShowPermissions = false
	return saveGame{
		game:        game,
		width:       width,
		height:      height,
		styles:      styles,
		returnModel: returnModel,
		dir:         dir,
		err:         err,
		gameName:    gameName,
		inputField:  inputField,
		filePicker:  filePicker,
	}
}

func (m *saveGame) save() bool {
	fileName := m.inputField.Value()
	if fileName == "" {
		m.message = "Please enter a name for the save game."
		return false
	}
	filePrefix := strings.ReplaceAll(m.gameName, " ", "_") + "-"
	fileSuffix := ""
	if !strings.HasSuffix(fileName, ".json") {
		fileSuffix = ".json"
	}
	fullName := fmt.Sprintf("%s%c%s%s%s", m.dir, os.PathSeparator, filePrefix, fileName, fileSuffix)
	file, err := os.Create(fullName)
	if err != nil {
		m.message = "Unable to write file: " + fullName
		return false
	}
	defer file.Close()
	err = engine.SaveGame(file, *m.game)
	if err != nil {
		m.message = "Error saving game: " + err.Error()
		return false
	}
	m.message = "Saved!"
	return true
}

func escapeAfter(t time.Duration) tea.Cmd {
	return tea.Tick(t, func(_ time.Time) tea.Msg {
		return tea.KeyMsg{Type: tea.KeyEscape}
	})
}

// Init is run once when the program starts
func (m saveGame) Init() tea.Cmd {
	return m.filePicker.Init()
}

// Update handles game state changes
func (m saveGame) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
	case tea.KeyMsg:
		switch msg.String() {
		case "esc":
			return m.returnModel, nil
		case "enter":
			success := m.save()
			if success {
				return m, escapeAfter(500 * time.Millisecond)
			}
			return m, nil
		case "ctrl+j":
			m.filePicker, cmd = m.filePicker.Update(tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{'J'}})
			return m, cmd
		case "ctrl+k":
			var cmd tea.Cmd
			m.filePicker, cmd = m.filePicker.Update(tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{'K'}})
			return m, cmd
		default:
			var cmd tea.Cmd
			m.inputField, cmd = m.inputField.Update(msg)
			return m, cmd
		}
	default:
		m.filePicker.Height = m.height - 13
		m.filePicker, cmd = m.filePicker.Update(msg)
	}
	return m, cmd
}

func (m saveGame) View() string {
	title := fmt.Sprintf("Save Game: %s", m.gameName)
	var titleStyle = lipgloss.NewStyle().
		Background(lipgloss.Color("62")).
		Foreground(lipgloss.Color("230")).
		Padding(0, 2).
		MarginLeft((m.width / 2) - len(title)/2)

	header := m.appBoundaryView("Save Game")
	nameLabel := "Name: "
	cmdInput := borderStyle.Height(1).Width(m.width - 4 - len(nameLabel)).Render(m.inputField.View())
	var body string
	if m.err != nil {
		body = titleStyle.MarginLeft(0).Render(fmt.Sprintf("Unable to find save directory: %s\n%s", m.dir, m.err.Error()))
	} else {
		message := ""
		if m.message != "" {
			message = headingStyle.MarginLeft(7).Render(m.message)
		}
		body = borderStyle.Width(m.width - 2).Height(m.height - 4).
			Render(
				lipgloss.JoinVertical(lipgloss.Top,
					titleStyle.Render(title),
					fmt.Sprintf("Directory: %s", m.dir),
					lipgloss.JoinHorizontal(lipgloss.Left, "\n"+nameLabel, cmdInput),
					message,
					titleStyle.MarginLeft(0).Render("Current Save Game Files:"),
					m.filePicker.View(),
				),
			)
	}
	footer := m.appBoundaryView("<ESC> to go back")

	return m.styles.Base.Render(lipgloss.JoinVertical(lipgloss.Top, header, body, footer))
}

func (m saveGame) appBoundaryView(text string) string {
	return lipgloss.PlaceHorizontal(m.width, lipgloss.Left, m.styles.HeaderText.Render("+-- "+text), lipgloss.WithWhitespaceChars("/"), lipgloss.WithWhitespaceForeground(indigo))
}
