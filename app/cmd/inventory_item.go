package main

import (
	"fmt"
	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"io"
	"strings"
)

var (
	itemStyle         = lipgloss.NewStyle().PaddingLeft(1)
	selectedItemStyle = lipgloss.NewStyle().PaddingLeft(1).Foreground(lipgloss.Color("170"))
)

type InventoryItem struct {
	title     string
	isWielded bool
}

func NewInventoryItem(title string, isWielded bool) InventoryItem {
	return InventoryItem{
		title:     title,
		isWielded: isWielded,
	}
}

func (t InventoryItem) FilterValue() string {
	return t.title
}

func (t InventoryItem) Title() string {
	if t.isWielded {
		return fmt.Sprintf("%s (⚔)", t.title)
	}
	return t.title
}

type itemDelegate struct{}

func (d itemDelegate) Height() int                             { return 1 }
func (d itemDelegate) Spacing() int                            { return 0 }
func (d itemDelegate) Update(_ tea.Msg, _ *list.Model) tea.Cmd { return nil }
func (d itemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(InventoryItem)
	if !ok {
		return
	}
	title := i.Title()
	str := fmt.Sprintf("%d. %s", index+1, title)

	fn := itemStyle.Render
	if index == m.Index() {
		fn = func(s ...string) string {
			return selectedItemStyle.Render(strings.Join(s, " "))
		}
	}

	_, err := fmt.Fprint(w, fn(str))
	if err != nil {
		return
	}
}
