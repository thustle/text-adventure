package main

import (
	"github.com/charmbracelet/bubbles/list"
	"strings"
	"testing"
)

func TestInventoryItem_Wielded(t *testing.T) {
	items := []list.Item{
		NewInventoryItem("Sword", false),
		NewInventoryItem("Pistol", true),
	}
	inventory := list.New([]list.Item{}, itemDelegate{}, 20, 8)
	inventory.SetItems(items)
	inventory.SetHeight(10)
	inventory.SetWidth(50)
	listRender := inventory.View()
	if !strings.Contains(listRender, "Sword") {
		t.Errorf("Unable to find sword in inventory")
	}
	if !strings.Contains(listRender, "Pistol (⚔)") {
		t.Errorf("Unable to find wielded pistol in inventory")
	}
}
