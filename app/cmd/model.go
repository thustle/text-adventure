package main

import (
	"fmt"
	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/progress"
	"github.com/charmbracelet/bubbles/textinput"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/lipgloss/table"
	"github.com/muesli/reflow/wordwrap"
	"math"
	"strings"
	"text-adventure/pkg/engine"
	"time"
)

const (
	UpdateDesc = iota
	NoUpdateDesc
)

const defaultGameName = "Text Adventure"

var (
	borderStyle = lipgloss.NewStyle().
			Padding(0, 0).
			Border(lipgloss.RoundedBorder()).
			BorderForeground(lipgloss.Color("62"))
	allowedDir    = lipgloss.NewStyle().Foreground(lipgloss.Color("70"))
	disallowedDir = lipgloss.NewStyle().Foreground(lipgloss.Color("173"))
	commandStyle  = lipgloss.NewStyle().Foreground(lipgloss.Color("105"))
)

// GameState holds the current state of the game
type GameState struct {
	descState   int
	currentGame *engine.Game
	printDesc   bool
}

// Model is the struct for the entire game model
type Model struct {
	gameState   *GameState
	lg          *lipgloss.Renderer
	styles      *Styles
	width       int
	height      int
	inputField  textinput.Model
	descContent string
	health      progress.Model
	contentPort *viewport.Model
	items       *list.Model
	history     *[]string
	historyPos  int
}

type healthTickMsg time.Time

func healthTickCmd() tea.Cmd {
	return tea.Tick(time.Millisecond*200, func(t time.Time) tea.Msg {
		return healthTickMsg(t)
	})
}

func createItemsList() *list.Model {
	items := list.New([]list.Item{}, itemDelegate{}, 20, 8)
	items.SetShowHelp(false)
	items.SetShowPagination(true)
	items.SetShowStatusBar(false)
	items.SetShowFilter(false)
	items.Title = "Inventory"
	return &items
}

func updateItemsList(m *Model, game *engine.Game) tea.Cmd {
	items := make([]list.Item, len(game.Status.UserObjects))
	for i, obj := range game.Status.UserObjects {
		isWielded := game.Status.SelectedWeaponID != nil && *game.Status.SelectedWeaponID == obj.ID
		items[i] = NewInventoryItem(obj.Name, isWielded)
	}
	return m.items.SetItems(items)
}

func NewModel() Model {
	m := Model{width: maxWidth}
	m.lg = lipgloss.DefaultRenderer()
	m.styles = NewStyles(m.lg)
	m.inputField = textinput.New()
	m.inputField.Focus()
	m.health = progress.New(progress.WithScaledGradient("#FF7CCB", "#9DFF8C"))
	m.items = createItemsList()
	game := engine.NewGame()
	gameState := GameState{
		currentGame: &game,
		printDesc:   true,
	}
	m.gameState = &gameState
	updateItemsList(&m, m.gameState.currentGame)
	addStateToDescription(&m, m.gameState.currentGame)
	m.history = &[]string{}
	return m
}

func (m *Model) updateGame(game *engine.Game) {
	gameState := GameState{
		currentGame: game,
		printDesc:   true,
	}
	m.gameState = &gameState
	m.inputField.SetValue("")
	m.descContent = ""
	updateItemsList(m, m.gameState.currentGame)
	addStateToDescription(m, m.gameState.currentGame)
	m.history = &[]string{}
}

// Init is run once when the program starts
func (m Model) Init() tea.Cmd {
	return healthTickCmd()
}

// Update handles game state changes
func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
		listHeight := m.height - 17
		m.items.SetHeight(listHeight)
	case progress.FrameMsg:
		// FrameMsg is sent when the health bar wants to animate itself
		progressModel, cmd := m.health.Update(msg)
		m.health = progressModel.(progress.Model)
		return m, cmd
	case healthTickMsg:
		commands := make([]tea.Cmd, 0)
		gameHealthPercent := int(m.gameState.currentGame.Status.Health)
		meterHealthPercent := int(math.Round(m.health.Percent() * 100))
		if meterHealthPercent < gameHealthPercent {
			cmd := m.health.IncrPercent(float64(gameHealthPercent-meterHealthPercent) / 100)
			commands = append(commands, healthTickCmd(), cmd)
		} else if meterHealthPercent > gameHealthPercent {
			cmd := m.health.DecrPercent(float64(meterHealthPercent-gameHealthPercent) / 100)
			commands = append(commands, healthTickCmd(), cmd)
		}
		return m, tea.Batch(commands...)
	case loadMessage:
		m.updateGame(msg.game)
		return m, healthTickCmd()
	case tea.KeyMsg:
		commands := make([]tea.Cmd, 0)
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		case "ctrl+h":
			help := newHelp(m.width, m.height, m.styles, m)
			return help, nil
		case "ctrl+l":
			load := newLoadGame(m.width, m.height, m.styles, m)
			cmd := load.Init()
			return load, cmd
		case "ctrl+s":
			save := newSaveGame(m.gameState.currentGame, m.width, m.height, m.styles, m, m.gameState.currentGame.GameName)
			cmd := save.Init()
			return save, cmd
		case "ctrl+j":
			m.items.Paginator.NextPage()
			_, cmd := m.items.Update(nil)
			commands = append(commands, cmd)
		case "ctrl+k":
			m.items.Paginator.PrevPage()
			_, cmd := m.items.Update(nil)
			commands = append(commands, cmd)
		case "enter":
			command := parseCommand(&m, msg)
			commands = append(commands, command)
		case "up":
			if m.historyPos > 0 {
				m.historyPos--
			}
			if m.historyPos >= 0 && m.historyPos < len(*m.history) {
				m.inputField.SetValue((*m.history)[m.historyPos])
			}
		case "down":
			if m.historyPos < len(*m.history) {
				m.historyPos++
			}
			if m.historyPos >= 0 && m.historyPos < len(*m.history) {
				m.inputField.SetValue((*m.history)[m.historyPos])
			} else if m.historyPos >= len(*m.history) {
				m.inputField.SetValue("")
			}
		case "shift+up":
			m.inputField.SetValue("go north")
			command := parseCommand(&m, msg)
			commands = append(commands, command)
		case "shift+down":
			m.inputField.SetValue("go south")
			command := parseCommand(&m, msg)
			commands = append(commands, command)
		case "shift+right":
			m.inputField.SetValue("go east")
			command := parseCommand(&m, msg)
			commands = append(commands, command)
		case "shift+left":
			m.inputField.SetValue("go west")
			command := parseCommand(&m, msg)
			commands = append(commands, command)
		default:
			var cmd tea.Cmd
			m.inputField, cmd = m.inputField.Update(msg)
			commands = append(commands, cmd)
		}
		if m.gameState.currentGame.Status.Health != engine.Health(math.Round(m.health.Percent()*100)) {
			commands = append(commands, healthTickCmd())
		}
		commands = append(commands, updateItemsList(&m, m.gameState.currentGame))
		addStateToDescription(&m, m.gameState.currentGame)
		return m, tea.Batch(commands...)
	}

	return m, nil
}

func parseCommand(m *Model, msg tea.Msg) tea.Cmd {
	command := m.inputField.Value()
	m.inputField.Focus()
	m.inputField.Reset()
	_, inputCmd := m.inputField.Update(msg)

	switch strings.ToLower(command) {
	case "quit":
		return tea.Quit
	case "clear":
		m.descContent = ""
		m.gameState.descState = UpdateDesc
		return nil
	}

	addToHistory(m, command)
	addToDescription(m, command, true)
	responseContent, result, err := m.gameState.currentGame.ParseCommand(command)
	if err != nil {
		addToDescription(m, "Error: "+err.Error(), false)
	}
	switch responseContent {
	case engine.ResponseEmpty:
		m.gameState.descState = UpdateDesc
	case engine.ResponseText:
		if result != "" {
			addToDescription(m, result, false)
		}
	}
	return tea.Batch(inputCmd)
}

func addToHistory(m *Model, command string) {
	*m.history = append(*m.history, command)
	m.historyPos = len(*m.history)
}

func addToDescription(m *Model, text string, userInput bool) {
	if userInput {
		m.descContent = m.descContent + "\n" + commandStyle.Render("> "+text) + "\n"
	} else {
		m.descContent = m.descContent + text
	}
	if m.contentPort != nil {
		m.contentPort.SetContent(wordwrap.String(m.descContent, m.width-26))
		m.contentPort.GotoBottom()
	}
}

func addStateToDescription(m *Model, game *engine.Game) {
	if m.gameState.descState == NoUpdateDesc {
		return
	}
	m.gameState.descState = NoUpdateDesc

	room := game.Rooms[game.Status.LocationId]
	m.descContent = m.descContent + room.DescriptionForUser(game.Status) + "\n" + "You can see " + describeObjects(room.ObjectsForUser(game.Status)) + "\n" +
		"You can move " + describeDirections(room.LocationsForUser(game.Status))
	if len(room.People) > 0 {
		for _, person := range room.People {
			m.descContent = m.descContent + fmt.Sprintf("\n%s", person.RoomDescription)
		}
	}
	if len(room.Enemies) > 0 {
		for _, enemy := range room.Enemies {
			m.descContent = m.descContent + fmt.Sprintf("\n%s", enemy.RoomDescription)
		}
	}
	if m.contentPort != nil {
		m.contentPort.SetContent(wordwrap.String(m.descContent, m.width-26))
		m.contentPort.GotoBottom()
	}
}

func describeObjects(objects []*engine.Object) string {
	if len(objects) == 0 {
		return "no items in this location"
	}
	builder := strings.Builder{}
	for i, object := range objects {
		if i > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(object.Name)
	}
	return builder.String()
}

func describeDirections(directions map[engine.Direction]string) string {
	if len(directions) == 0 {
		return "nowhere!"
	}
	builder := strings.Builder{}
	i := 0
	for dir := range directions {
		if i > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(translateDirection(dir))
		i++
	}
	return builder.String()
}

func translateDirection(dir engine.Direction) string {
	switch dir {
	case engine.North:
		return "North"
	case engine.South:
		return "South"
	case engine.East:
		return "East"
	default:
		return "West"
	}
}

// View displays the current game state
func (m Model) View() string {
	m.health.Width = 20
	listHeight := m.height - 17
	m.items.SetHeight(listHeight)

	gameName := defaultGameName
	if m.gameState.currentGame.GameName != "" {
		gameName = m.gameState.currentGame.GameName
	}
	header := m.appBoundaryView([]string{"Text Adventure", gameName})
	footer := m.appBoundaryView([]string{"Ctrl+h to view help", "Ctrl+s to save", "Ctrl+l to load"})

	panel1 := borderStyle.Width(20).Height(2).Render(titleStyle.Render("Health") + "\n" + m.health.View())
	panel2 := borderStyle.Width(20).Height(6).Render(m.items.View())
	directionTable := table.New()
	directionTable.Width(8)
	directionTable.BorderTop(false)
	directionTable.BorderBottom(false)
	directionTable.BorderLeft(false)
	directionTable.BorderRight(false)
	directionTable.BorderColumn(false)
	room := m.gameState.currentGame.Rooms[m.gameState.currentGame.Status.LocationId]
	roomLocations := room.LocationsForUser(m.gameState.currentGame.Status)
	northAllowed := directionStyle(roomLocations[engine.North] != "")
	southAllowed := directionStyle(roomLocations[engine.South] != "")
	eastAllowed := directionStyle(roomLocations[engine.East] != "")
	westAllowed := directionStyle(roomLocations[engine.West] != "")
	keys := [][]string{
		{" ", northAllowed.Render("N"), " "},
		{westAllowed.Render("W"), "+", eastAllowed.Render("E")},
		{" ", southAllowed.Render("S"), " "},
	}
	directionTable.Rows(keys...)
	directionContent := lipgloss.PlaceHorizontal(18, lipgloss.Center, directionTable.Render())
	panel3 := borderStyle.Width(20).Height(4).Render(titleStyle.Render("Directions") + "\n" + directionContent)
	sidePanel := lipgloss.JoinVertical(lipgloss.Top, panel1, panel2, panel3)

	vpHeight := 18
	if m.height > 7 {
		vpHeight = m.height - 7
	}
	if m.contentPort == nil {
		vp := viewport.New(m.width-26, vpHeight)
		m.contentPort = &vp
		m.contentPort.SetContent(wordwrap.String(m.descContent, m.width-26))
		m.contentPort.GotoBottom()
	}
	cmdDesc := borderStyle.Width(m.width - 24).Height(m.height - 7).Render(m.contentPort.View())
	mainPanel := lipgloss.JoinHorizontal(lipgloss.Left, cmdDesc, sidePanel)
	cmdInput := borderStyle.Height(1).Width(m.width - 2).Render(m.inputField.View())
	body := lipgloss.JoinVertical(lipgloss.Top, mainPanel, cmdInput)

	return m.styles.Base.Render(header + "\n" + body + "\n" + footer)
}

func (m Model) appBoundaryView(textItems []string) string {
	builder := strings.Builder{}
	builder.WriteString(m.styles.HeaderDecoration.Render("+--"))
	for i, textItem := range textItems {
		if i > 0 {
			builder.WriteString(m.styles.HeaderDecoration.Render("/////"))
		}
		builder.WriteString(m.styles.HeaderText.Render(textItem))
	}
	return lipgloss.PlaceHorizontal(m.width, lipgloss.Left, builder.String(), lipgloss.WithWhitespaceChars("/"), lipgloss.WithWhitespaceForeground(indigo))
}

func (m Model) appErrorBoundaryView(text string) string {
	return lipgloss.PlaceHorizontal(m.width, lipgloss.Left, m.styles.ErrorHeaderText.Render(text), lipgloss.WithWhitespaceChars("/"), lipgloss.WithWhitespaceForeground(red))
}

func directionStyle(allowed bool) lipgloss.Style {
	if allowed {
		return allowedDir
	} else {
		return disallowedDir
	}
}
