package main

import (
	"fmt"
	"github.com/charmbracelet/bubbles/filepicker"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"os"
	"text-adventure/pkg/engine"
)

type loadGame struct {
	width       int
	height      int
	styles      *Styles
	returnModel tea.Model
	dir         string
	err         error
	message     string
	filePicker  filepicker.Model
}

func newLoadGame(width, height int, styles *Styles, returnModel tea.Model) loadGame {
	dir, err := getAppDataPath()
	filePicker := filepicker.New()
	filePicker.AllowedTypes = []string{".sav", ".json"}
	filePicker.CurrentDirectory = dir
	filePicker.ShowPermissions = false
	return loadGame{
		width:       width,
		height:      height,
		styles:      styles,
		returnModel: returnModel,
		dir:         dir,
		err:         err,
		message:     "",
		filePicker:  filePicker,
	}
}

type loadMessage struct {
	game *engine.Game
}

func loadCmd(game *engine.Game) tea.Cmd {
	return func() tea.Msg {
		return loadMessage{game: game}
	}
}

func (m *loadGame) load(selectedFile string) (*engine.Game, bool) {
	if selectedFile == "" {
		m.message = "Please pick a file to load."
		return nil, false
	}
	file, err := os.Open(selectedFile)
	if err != nil {
		m.message = "Unable to open file: " + selectedFile
		return nil, false
	}
	defer file.Close()
	game, err := engine.LoadGame(file)
	if err != nil {
		m.message = "Error loading game: " + err.Error()
		return nil, false
	}
	return &game, true
}

// Init is run once when the program starts
func (m loadGame) Init() tea.Cmd {
	return m.filePicker.Init()
}

// Update handles game state changes
func (m loadGame) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
	case tea.KeyMsg:
		switch msg.String() {
		case "esc":
			return m.returnModel, nil
		default:
			m.filePicker.Height = m.height - 13
			m.filePicker, cmd = m.filePicker.Update(msg)
		}
	default:
		m.filePicker.Height = m.height - 13
		m.filePicker, cmd = m.filePicker.Update(msg)
	}
	if didSelect, path := m.filePicker.DidSelectFile(msg); didSelect {
		game, success := m.load(path)
		if success {
			return m.returnModel, loadCmd(game)
		}
	}
	return m, cmd
}

func (m loadGame) View() string {
	title := "Load Game"
	var titleStyle = lipgloss.NewStyle().
		Background(lipgloss.Color("62")).
		Foreground(lipgloss.Color("230")).
		Padding(0, 2).
		MarginLeft((m.width / 2) - len(title)/2)

	header := m.appBoundaryView("Load Game")
	var body string
	if m.err != nil {
		body = titleStyle.MarginLeft(0).Render(fmt.Sprintf("Unable to find save directory: %s\n%s", m.dir, m.err.Error()))
	} else {
		message := ""
		if m.message != "" {
			message = headingStyle.MarginLeft(7).Render(m.message)
		}
		body = borderStyle.Width(m.width - 2).Height(m.height - 4).
			Render(
				lipgloss.JoinVertical(lipgloss.Top,
					titleStyle.Render(title),
					fmt.Sprintf("Directory: %s", m.dir),
					message,
					titleStyle.MarginLeft(0).Render("Current Save Game Files:"),
					m.filePicker.View(),
				),
			)
	}
	footer := m.appBoundaryView("<ESC> to go back")

	return m.styles.Base.Render(lipgloss.JoinVertical(lipgloss.Top, header, body, footer))
}

func (m loadGame) appBoundaryView(text string) string {
	return lipgloss.PlaceHorizontal(m.width, lipgloss.Left, m.styles.HeaderText.Render("+-- "+text), lipgloss.WithWhitespaceChars("/"), lipgloss.WithWhitespaceForeground(indigo))
}
