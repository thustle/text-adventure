package main

import (
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/x/exp/teatest"
	"github.com/muesli/termenv"
	"io"
	"testing"
	"time"
)

func init() {
	lipgloss.SetColorProfile(termenv.Ascii)
}

func TestModelOutput(t *testing.T) {
	model := NewModel()
	tm := teatest.NewTestModel(t, model, teatest.WithInitialTermSize(80, 25))
	time.Sleep(100 * time.Millisecond)
	out, err := io.ReadAll(tm.Output())
	if err != nil {
		t.Error(err)
	}
	teatest.RequireEqualOutput(t, out)
}
