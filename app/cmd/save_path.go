package main

import (
	"fmt"
	"os"
)

func getAppDataPath() (string, error) {
	var dir string
	userDir, err := os.UserConfigDir()
	dir = fmt.Sprintf("%s%c%s", userDir, os.PathSeparator, "text-adventure")
	_, err = os.Stat(dir)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(dir, os.ModePerm)
			if err != nil {
				return dir, err
			}
		}
		return dir, err
	}
	return dir, nil
}
