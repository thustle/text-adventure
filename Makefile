.PHONY: create-demo
create-demo: build
	vhs ./readme/demo.tape

.PHONY: build
build:
	go build -o bin/text-adventure ./app/cmd

.PHONY: run
run: build
	./bin/text-adventure

.PHONY: test
test:
	go test -v ./...
