package engine

type PersonInteractionType int

const (
	PersonInteractionGive PersonInteractionType = iota
)

type PersonInteraction struct {
	ObjectID        string                `json:"objectId"`
	Type            PersonInteractionType `json:"type"`
	Description     string                `json:"description"`
	Success         bool                  `json:"success"`
	ReplacePerson   bool                  `json:"replacePerson"`
	ReplaceOrigin   bool                  `json:"replaceOrigin"`
	ReceiveObjects  []Object              `json:"receiveObjects"`
	OpenDirections  []direction           `json:"openDirections"`
	CloseDirections []Direction           `json:"closeDirections"`
	NewRoomDesc     string                `json:"newRoomDescription"`
	NewLines        []string              `json:"newTalkLines"`
}

type Person struct {
	ID              string              `json:"id"`
	Name            string              `json:"name"`
	Description     string              `json:"description"`
	RoomDescription string              `json:"roomDescription"`
	TalkLines       []string            `json:"talkLines"`
	Interactions    []PersonInteraction `json:"interactions"`
}
