package engine

import (
	"reflect"
	"testing"
)

func TestRoom_LocationsForUser(t *testing.T) {
	tests := []struct {
		name   string
		room   *Room
		status *Status
		want   map[Direction]string
	}{
		{
			name: "Room Locations with no mutations",
			room: &Room{
				Locations: map[Direction]string{
					North: "room1",
					West:  "room2",
				},
				Mutations: []*RoomMutation{},
			},
			status: &Status{UserObjects: []Object{}},
			want: map[Direction]string{
				North: "room1",
				West:  "room2",
			},
		},
		{
			name: "Room Locations overridden by an player object mutation",
			room: &Room{
				Locations: map[Direction]string{
					North: "room1",
					West:  "room2",
				},
				Mutations: []*RoomMutation{
					{
						Locations: map[Direction]string{
							South: "room3",
						},
						ObjectID: "pickaxe",
					},
				},
			},
			status: &Status{UserObjects: []Object{{ID: "pickaxe"}}},
			want: map[Direction]string{
				South: "room3",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.room.LocationsForUser(tt.status); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Room.LocationsForUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRoom_DescriptionForUser(t *testing.T) {
	r := &Room{
		Description: "Test room description",
		Mutations: []*RoomMutation{
			{
				Description: "Mutated room description",
				ObjectID:    "1",
			},
		},
	}
	statusWithMutation := &Status{
		UserObjects: []Object{
			{ID: "1"},
		},
	}
	statusWithoutMutation := &Status{}

	tests := []struct {
		name   string
		r      *Room
		status *Status
		want   string
	}{
		{
			name:   "Location description overridden with player object mutation",
			r:      r,
			status: statusWithMutation,
			want:   "Mutated room description",
		},
		{
			name:   "Location description without any mutations",
			r:      r,
			status: statusWithoutMutation,
			want:   "Test room description",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.DescriptionForUser(tt.status); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Room.DescriptionForUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRoom_ObjectsForUser(t *testing.T) {
	tests := []struct {
		name   string
		room   *Room
		status *Status
		want   []*Object
	}{
		{
			name:   "Empty room",
			room:   &Room{},
			status: &Status{},
			want:   []*Object{},
		},
		{
			name: "Room with object and no mutations",
			room: &Room{
				Objects: []*Object{
					{ID: "obj1"},
				},
			},
			status: &Status{UserObjects: []Object{{ID: "obj2"}}},
			want: []*Object{
				{ID: "obj1"},
			},
		},
		{
			name: "Room with non-matching mutation",
			room: &Room{
				Objects: []*Object{
					{ID: "obj1"},
				},
				Mutations: []*RoomMutation{
					{ObjectID: "obj2", Objects: []*Object{{ID: "obj2m"}}},
					{ObjectID: "obj3", Objects: []*Object{{ID: "obj3m"}}},
				},
			},
			status: &Status{UserObjects: []Object{{ID: "obj4"}, {ID: "obj5"}}},
			want: []*Object{
				{ID: "obj1"},
			},
		},
		{
			name: "Room with matching mutation",
			room: &Room{
				Objects: []*Object{
					{ID: "obj1"},
				},
				Mutations: []*RoomMutation{
					{ObjectID: "obj2", Objects: []*Object{{ID: "obj2m"}}},
					{ObjectID: "obj3", Objects: []*Object{{ID: "obj3m"}}},
					{ObjectID: "obj4", Objects: []*Object{{ID: "obj4m"}}},
				},
			},
			status: &Status{UserObjects: []Object{{ID: "obj2"}, {ID: "obj3"}, {ID: "obj5"}}},
			want: []*Object{
				{ID: "obj1"},
				{ID: "obj2m"},
				{ID: "obj3m"},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.room.ObjectsForUser(tt.status); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Room.ObjectsForUser() = %v, want %v", got, tt.want)
			}
		})
	}
}
