package engine

const (
	notKnown = "I'm sorry, I do not understand."
	help     = "Press CTRL+H to view the help screen."
)
