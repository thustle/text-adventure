package engine

import (
	"golang.org/x/exp/constraints"
	"testing"
)

type healthRange[T constraints.Integer] struct {
	min, max T
}

func TestEnemy_ReceiveDamage(t *testing.T) {
	tests := []struct {
		name                  string
		enemy                 *Enemy
		attackStrength        uint
		wantDamageReceived    healthRange[int]
		wantHealthAfterAttack healthRange[int]
	}{
		{name: "MinimumAttack", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 10}, attackStrength: 0, wantDamageReceived: healthRange[int]{min: 0, max: 0}, wantHealthAfterAttack: healthRange[int]{min: 100, max: 100}},
		{name: "MaximumAttack", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 10}, attackStrength: 100, wantDamageReceived: healthRange[int]{min: 70, max: 130}, wantHealthAfterAttack: healthRange[int]{min: 0, max: 30}},
		{name: "MoreThanMaxHealth", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 10}, attackStrength: 200, wantDamageReceived: healthRange[int]{min: 140, max: 260}, wantHealthAfterAttack: healthRange[int]{min: 0, max: 0}},
		{name: "LowHealth", enemy: &Enemy{Name: "Enemy1", Health: 10, Attack: 10}, attackStrength: 10, wantDamageReceived: healthRange[int]{min: 7, max: 13}, wantHealthAfterAttack: healthRange[int]{min: 0, max: 3}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDamageAmount := tt.enemy.ReceiveDamage(tt.attackStrength)
			if gotDamageAmount < tt.wantDamageReceived.min || gotDamageAmount > tt.wantDamageReceived.max {
				t.Errorf("ReceiveDamage() Damage = %v, want %v", gotDamageAmount, tt.wantDamageReceived)
			}

			if tt.enemy.Health < tt.wantHealthAfterAttack.min || tt.enemy.Health > tt.wantHealthAfterAttack.max {
				t.Errorf("ReceiveDamage() Health After Attack = %v, want %v", tt.enemy.Health, tt.wantHealthAfterAttack)
			}
		})
	}
}

func TestEnemy_GetAttackDamage(t *testing.T) {
	tests := []struct {
		name             string
		enemy            *Enemy
		wantAttackDamage healthRange[uint]
	}{
		{name: "SmallAttack", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 10}, wantAttackDamage: healthRange[uint]{min: 7, max: 13}},
		{name: "LargeAttack", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 100}, wantAttackDamage: healthRange[uint]{min: 70, max: 130}},
		{name: "LargerHealth", enemy: &Enemy{Name: "Enemy1", Health: 100, Attack: 200}, wantAttackDamage: healthRange[uint]{min: 140, max: 260}},
		{name: "MidAttack", enemy: &Enemy{Name: "Enemy1", Health: 10, Attack: 50}, wantAttackDamage: healthRange[uint]{min: 35, max: 65}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotAttackDamage := tt.enemy.GetAttackDamage()
			if gotAttackDamage < tt.wantAttackDamage.min || gotAttackDamage > tt.wantAttackDamage.max {
				t.Errorf("AttackDamage() Damage = %v, want %v", gotAttackDamage, tt.wantAttackDamage)
			}
		})
	}
}
