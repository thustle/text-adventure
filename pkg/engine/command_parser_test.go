package engine

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func TestParseCommand(t *testing.T) {
	tests := []struct {
		name         string
		gameState    *Game
		cmd          string
		wantResponse ResponseContent
		wantText     string
		wantErr      bool
	}{
		{
			name: "Test when player is dead",
			gameState: &Game{
				Status: &Status{
					Health: 0,
				},
			},
			cmd:          "",
			wantResponse: ResponseText,
			wantText:     "You are dead. Game over.",
			wantErr:      false,
		},
		{
			name: "Test when player has won",
			gameState: &Game{
				Status: &Status{
					Health: 100,
					Won:    true,
				},
			},
			cmd:          "",
			wantResponse: ResponseText,
			wantText:     "You won. Game over.",
			wantErr:      false,
		},
		{
			name: "Test when player wins",
			gameState: &Game{
				Status: &Status{
					LocationId: "LOC1",
					Health:     100,
				},
				Rooms: map[string]*Room{
					"LOC1": {
						Locations: map[Direction]string{
							South: "LOC2",
						},
					},
					"LOC2": {
						WinGame: true,
					},
				},
			},
			cmd:          "go south",
			wantResponse: ResponseEmpty,
			wantText:     "You have successfully completed your quest",
			wantErr:      false,
		},
		{
			name: "Test when player dies",
			gameState: &Game{
				Status: &Status{
					LocationId: "LOC1",
					Health:     1,
				},
				Rooms: map[string]*Room{
					"LOC1": {
						Enemies: []*Enemy{
							{
								Name:   "Orc",
								Health: 100,
								Attack: 10,
							},
						},
					},
				},
			},
			cmd:          "hit orc",
			wantResponse: ResponseText,
			wantText:     "Game over",
			wantErr:      false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResponse, gotText, err := parseCommand(tt.gameState, tt.cmd)

			if (err != nil) != tt.wantErr {
				t.Errorf("parseCommand() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotResponse != tt.wantResponse || !strings.Contains(gotText, tt.wantText) {
				t.Errorf("parseCommand() got = %v, %v, want = %v, %v", gotResponse, gotText, tt.wantResponse, tt.wantText)
			}
		})
	}
}

func TestParseHelp(t *testing.T) {
	gotResp, gotDesc, _, err := parseWords(&Game{}, "help")
	if err != nil {
		t.Errorf("parseHelp() unexpected error: %s", err)
	}
	if gotResp != ResponseText {
		t.Errorf("parseHelp() gotResp = %v, want %v", gotResp, ResponseText)
	}
	if gotDesc != help {
		t.Errorf("parseHelp() gotDesc = %v, want %v", gotDesc, help)
	}
}

func TestParseGet(t *testing.T) {
	obj := &Object{ID: "obj1", Name: "obj1", Description: "object", Obtainable: true}
	obj2 := &Object{ID: "obj2", Name: "obj2", Description: "object2", Obtainable: true}

	tests := []struct {
		name            string
		game            *Game
		words           string
		wantResp        ResponseContent
		wantDesc        string
		wantUserObjects []Object
	}{
		{
			name: "get no words",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{}},
				Rooms:  map[string]*Room{"home": {}},
			},
			words:           "get",
			wantResp:        ResponseText,
			wantDesc:        "What?",
			wantUserObjects: []Object{},
		},
		{
			name: "pick up no words",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{}},
				Rooms:  map[string]*Room{"home": {}},
			},
			words:           "pick up",
			wantResp:        ResponseText,
			wantDesc:        "What?",
			wantUserObjects: []Object{},
		},
		{
			name: "get object",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{}},
				Rooms: map[string]*Room{
					"home": {Objects: []*Object{obj}},
				},
			},
			words:           fmt.Sprintf("get %s", obj.Name),
			wantResp:        ResponseText,
			wantDesc:        "You pick up the " + obj.Name,
			wantUserObjects: []Object{*obj},
		},
		{
			name: "pick up object",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {Objects: []*Object{obj}},
				},
			},
			words:           fmt.Sprintf("pick up %s", obj.Name),
			wantResp:        ResponseText,
			wantDesc:        "You pick up the " + obj.Name,
			wantUserObjects: []Object{*obj},
		},
		{
			name: "pick up mutation object",
			game: &Game{
				Status: &Status{
					LocationId:  "home",
					UserObjects: []Object{*obj},
				},
				Rooms: map[string]*Room{
					"home": {
						Mutations: []*RoomMutation{
							{
								ObjectID: obj.ID,
								Objects:  []*Object{obj2},
							},
						},
					},
				},
			},
			words:           fmt.Sprintf("pick up %s", obj2.Name),
			wantResp:        ResponseText,
			wantDesc:        "You pick up the " + obj2.Name,
			wantUserObjects: []Object{*obj, *obj2},
		},
		{
			name: "item not found",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{}},
				Rooms: map[string]*Room{
					"home": {},
				},
			},
			words:           "pick up nonexistent",
			wantResp:        ResponseText,
			wantDesc:        "I can't see that here",
			wantUserObjects: []Object{},
		},
		{
			name: "item not pickup-able",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{}},
				Rooms: map[string]*Room{
					"home": {
						Objects: []*Object{{Name: "boulder", Obtainable: false}},
					},
				},
			},
			words:           "pick up boulder",
			wantResp:        ResponseText,
			wantDesc:        "You can't pick that up",
			wantUserObjects: []Object{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResp, gotDesc, canBeAttacked, _ := parseWords(tt.game, tt.words)
			if !canBeAttacked {
				t.Errorf("parseGet() error, expected canBeAttacked == false")
				return
			}
			if !reflect.DeepEqual(tt.game.Status.UserObjects, tt.wantUserObjects) {
				t.Errorf("parseGet() user objects = %v, wantUserObjects %v", tt.game.Status.UserObjects, tt.wantUserObjects)
				return
			}
			if gotResp != tt.wantResp {
				t.Errorf("parseGet() gotResp = %v, want %v", gotResp, tt.wantResp)
			}
			if !strings.Contains(gotDesc, tt.wantDesc) {
				t.Errorf("parseGet() gotDesc = %v, want %v", gotDesc, tt.wantDesc)
			}
		})
	}
}

func TestParseMove(t *testing.T) {
	obj := &Object{ID: "obj1", Name: "obj1", Description: "object", Obtainable: true}
	rooms := map[string]*Room{
		"home": {
			Description: "home",
			Locations: map[Direction]string{
				West: "garden",
			},
			Mutations: []*RoomMutation{
				{
					ObjectID: obj.ID,
					Locations: map[Direction]string{
						North: "forest",
					},
				},
			},
		},
		"garden": {
			Description: "garden",
			Locations: map[Direction]string{
				East: "home",
			},
		},
		"forest": {
			Description: "forest",
		},
	}

	tests := []struct {
		name             string
		game             *Game
		words            string
		wantResp         ResponseContent
		wantUserLocation string
	}{
		{
			name: "move no words",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms:  rooms,
			},
			words:            "move",
			wantResp:         ResponseText,
			wantUserLocation: "home",
		},
		{
			name: "move unknown",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms:  rooms,
			},
			words:            "move along",
			wantResp:         ResponseText,
			wantUserLocation: "home",
		},
		{
			name: "move invalid",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms:  rooms,
			},
			words:            "move east",
			wantResp:         ResponseText,
			wantUserLocation: "home",
		},
		{
			name: "move valid",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms:  rooms,
			},
			words:            "move west",
			wantResp:         ResponseEmpty,
			wantUserLocation: "garden",
		},
		{
			name: "move mutation invalid",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{*obj}},
				Rooms:  rooms,
			},
			words:            "move west",
			wantResp:         ResponseText,
			wantUserLocation: "home",
		},
		{
			name: "move mutation valid",
			game: &Game{
				Status: &Status{LocationId: "home", UserObjects: []Object{*obj}},
				Rooms:  rooms,
			},
			words:            "move north",
			wantResp:         ResponseEmpty,
			wantUserLocation: "forest",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResp, _, canBeAttacked, _ := parseWords(tt.game, tt.words)
			if canBeAttacked {
				t.Errorf("parseMove() error, expected canBeAttacked == false")
				return
			}
			if tt.game.Status.LocationId != tt.wantUserLocation {
				t.Errorf("parseMove() user location = %v, wantUserLocation %v", tt.game.Status.UserObjects, tt.wantUserLocation)
				return
			}
			if gotResp != tt.wantResp {
				t.Errorf("parseMove() gotResp = %v, want %v", gotResp, tt.wantResp)
			}
		})
	}
}

func TestParseWield(t *testing.T) {
	tests := []struct {
		name     string
		words    []string
		hasItem  bool
		isWeapon bool
		want     string
	}{
		{
			name:     "no word given",
			words:    []string{},
			hasItem:  false,
			isWeapon: false,
			want:     "What?",
		},
		{
			name:     "item doesn't exist",
			words:    []string{"sword"},
			hasItem:  false,
			isWeapon: false,
			want:     "You don't have that item.",
		},
		{
			name:     "item is not a weapon",
			words:    []string{"apple"},
			hasItem:  true,
			isWeapon: false,
			want:     "That item is not a weapon.",
		},
		{
			name:     "item is a weapon",
			words:    []string{"sword"},
			hasItem:  true,
			isWeapon: true,
			want:     "You wield the sword",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := &Game{
				Status: &Status{
					UserObjects: []Object{},
				},
			}

			if tt.hasItem {
				obj := Object{
					Name:         tt.words[0],
					AttackDamage: 0,
				}
				if tt.isWeapon {
					obj.AttackDamage = 10
					obj.IsWeapon = true
				}
				g.Status.UserObjects = append(g.Status.UserObjects, obj)
			}

			_, got, _, _ := parseWords(g, "wield "+strings.Join(tt.words, " "))
			if got != tt.want {
				t.Errorf("parseWield() = '%v', want '%v'", got, tt.want)
			}
		})
	}
}

func TestParseDrop(t *testing.T) {
	tests := []struct {
		name    string
		g       *Game
		words   []string
		wantRes ResponseContent
		wantMsg string
		wantErr error
	}{
		{
			name: "Empty Words",
			g: &Game{
				Status: &Status{},
				Rooms:  map[string]*Room{},
			},
			words:   []string{},
			wantRes: ResponseText,
			wantMsg: "What?",
			wantErr: nil,
		},
		{
			name: "Item Not In The User Objects",
			g: &Game{
				Status: &Status{
					UserObjects: []Object{},
					LocationId:  "room1",
				},
				Rooms: map[string]*Room{
					"room1": {},
				},
			},
			words:   []string{"randomItem"},
			wantRes: ResponseText,
			wantMsg: "You don't have that item.",
			wantErr: nil,
		},
		{
			name: "Item In The User Objects",
			g: &Game{
				Status: &Status{
					UserObjects: []Object{
						{Name: "randomItem"},
					},
					LocationId: "room1",
				},
				Rooms: map[string]*Room{
					"room1": {},
				},
			},
			words:   []string{"randomItem"},
			wantRes: ResponseText,
			wantMsg: "You drop the randomitem",
			wantErr: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, msg, _, err := parseWords(tt.g, "drop "+strings.Join(tt.words, " "))
			if res != tt.wantRes {
				t.Errorf("parseDrop() gotRes = %v, wantRes = %v", res, tt.wantRes)
			}
			if msg != tt.wantMsg {
				t.Errorf("parseDrop() gotMsg = %v, wantMsg = %v", msg, tt.wantMsg)
			}
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("parseDrop() gotErr = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}

func TestParseConsume(t *testing.T) {
	tests := []struct {
		name           string
		inputGame      *Game
		inputWords     []string
		healthBenefit  int
		expectedResp   ResponseContent
		expectedStr    string
		expectedError  error
		expectedHealth Health
	}{
		{
			name: "NoWordsProvided",
			inputGame: &Game{
				Status: &Status{Health: 50},
			},
			inputWords:     []string{},
			expectedResp:   ResponseText,
			expectedStr:    "What?",
			expectedError:  nil,
			expectedHealth: 50,
		},
		{
			name: "ItemNotPresent",
			inputGame: &Game{
				Status: &Status{
					UserObjects: []Object{},
					Health:      50,
				},
			},
			inputWords:     []string{"dummy_item"},
			expectedResp:   ResponseText,
			expectedStr:    "You don't have that item to consume.",
			expectedError:  nil,
			expectedHealth: 50,
		},
		{
			name: "NonConsumableObject",
			inputGame: &Game{
				Status: &Status{
					UserObjects: []Object{
						{Name: "dummy_item", Consumable: false},
					},
					Health: 50,
				},
			},
			inputWords:     []string{"dummy_item"},
			expectedResp:   ResponseText,
			expectedStr:    "Yuck, no way!",
			expectedError:  nil,
			expectedHealth: 50,
		},
		{
			name: "Consumable Object No Description",
			inputGame: &Game{
				Status: &Status{
					UserObjects: []Object{
						{Name: "dummy_item", Consumable: true, HealthBenefit: 10},
					},
					Health: 50,
				},
			},
			inputWords:     []string{"dummy_item"},
			expectedResp:   ResponseText,
			expectedStr:    "You consume the dummy_item",
			expectedError:  nil,
			expectedHealth: 60,
		},
		{
			name: "Consumable Object removing all health",
			inputGame: &Game{
				Status: &Status{
					UserObjects: []Object{
						{Name: "dummy_item", Consumable: true, HealthBenefit: -60},
					},
					Health: 50,
				},
			},
			inputWords:     []string{"dummy_item"},
			expectedResp:   ResponseText,
			expectedStr:    "You consume the dummy_item",
			expectedError:  nil,
			expectedHealth: 0,
		},
		{
			name: "Consumable Object With Description",
			inputGame: &Game{
				Status: &Status{
					UserObjects: []Object{
						{Name: "dummy_item", Consumable: true, HealthBenefit: 60, ConsumeDesc: "consume description"},
					},
					Health: 50,
				},
			},
			inputWords:     []string{"dummy_item"},
			expectedResp:   ResponseText,
			expectedStr:    "consume description",
			expectedError:  nil,
			expectedHealth: 100,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resp, str, _, err := parseWords(tt.inputGame, "eat "+strings.Join(tt.inputWords, " "))
			if resp != tt.expectedResp || str != tt.expectedStr || !errors.Is(err, tt.expectedError) {
				t.Errorf("parseConsume() = %v, %v, %v, want %v, %v, %v", resp, str, err, tt.expectedResp, tt.expectedStr, tt.expectedError)
			}
			if tt.inputGame.Status.Health != tt.expectedHealth {
				t.Errorf("parseConsume() = %v, want %v", tt.inputGame.Status.Health, tt.expectedHealth)
			}
		})
	}
}

func TestParseTalk(t *testing.T) {
	// Preset some data for testing
	people := []*Person{
		{Name: "Alice", TalkLines: []string{"Hello, I'm Alice."}},
		{Name: "Jo", TalkLines: []string{"Hi, I'm Jo."}},
		{Name: "Bob", TalkLines: []string{}},
	}
	enemies := []*Enemy{
		{Name: "Eve", TalkLines: []string{"I'm Eve."}},
		{Name: "Jason", TalkLines: []string{}},
	}
	rooms := map[string]*Room{
		"LOC1": {People: people, Enemies: enemies},
	}
	g := Game{Status: &Status{LocationId: "LOC1"}, Rooms: rooms}

	// Table driven test cases
	tests := []struct {
		name     string
		words    []string
		expected ResponseContent
		text     string
	}{
		{"talk to person", []string{"to", "Alice"}, ResponseText, "Hello, I'm Alice."},
		{"talk to person with no talk lines", []string{"to", "Bob"}, ResponseText, "You get no answer."},
		{"talk to non existing person", []string{"to", "Charlie"}, ResponseText, "I can't see that person."},
		{"talk to enemy", []string{"to", "Eve"}, ResponseText, "I'm Eve."},
		{"talk to enemy with no talk lines", []string{"to", "Jason"}, ResponseText, "You get no answer."},
		{"talk without specifying target", []string{}, ResponseText, "\"Hello\", you shout, but no one is listening."},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, text, _, err := parseWords(&g, "talk "+strings.Join(tt.words, " "))
			if err != nil {
				t.Fatal(err)
			}
			if res != tt.expected || text != tt.text {
				t.Fatalf("Expected: %v \"%s\", but got: %v \"%s\"", tt.expected, tt.text, res, text)
			}
		})
	}
}

func TestParseAttack(t *testing.T) {
	axe := Object{ID: "AXE", Name: "Axe", AttackDamage: 10}
	tests := []struct {
		name         string
		wielding     *Object
		words        []string
		wantDesc     string
		enemyRemoved bool
	}{
		{
			name:         "Test Attack enemy with fists",
			words:        []string{"Enemy1"},
			wantDesc:     "You hit out with your fists",
			enemyRemoved: true,
		},
		{
			name:         "Test Attack enemy with weapon",
			wielding:     &axe,
			words:        []string{"Enemy1"},
			wantDesc:     "You hit out with your Axe",
			enemyRemoved: true,
		},
		{
			name:     "Test Enemy Does Not Exist",
			words:    []string{"EnemyUnknown"},
			wantDesc: "That enemy isn't here.",
		},
		{
			name:     "Test No Enemy Specified",
			words:    []string{},
			wantDesc: "Who?",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			game := &Game{
				Status: &Status{LocationId: "LOC1", Health: 100, UserObjects: []Object{axe}},
				Rooms: map[string]*Room{
					"LOC1": {
						Enemies: []*Enemy{
							{
								Name:   "enemy1",
								Health: 1,
								Attack: 1,
							},
						},
					},
				},
			}
			if tt.wielding != nil {
				game.Status.SelectedWeaponID = &tt.wielding.ID
			}
			_, desc, _, _ := parseWords(game, "hit "+strings.Join(tt.words, " "))
			if !strings.Contains(desc, tt.wantDesc) {
				t.Errorf("parseAttack() = %v, want %v", desc, tt.wantDesc)
			}
			if tt.enemyRemoved && len(game.Rooms["LOC1"].Enemies) > 0 {
				t.Errorf("parseAttack() expected to not have any more enemies")
			}
		})
	}
}
