package engine

import (
	"bufio"
	"bytes"
	"github.com/go-test/deep"
	"testing"
	"text-adventure/pkg/gamefile"
)

func TestNewGame_DefaultGameSaveCompareFileJSON(t *testing.T) {
	g := NewGame()
	var savedBytes bytes.Buffer
	writer := bufio.NewWriter(&savedBytes)
	err := SaveGame(writer, g)
	_ = writer.Flush()
	if err != nil {
		t.Fatalf("Unable to save game due to %s\n", err)
	}

	defaultGameBytes := gamefile.DefaultGameJSON
	diff := deep.Equal(defaultGameBytes, savedBytes.Bytes())
	if diff != nil {
		t.Errorf("Original game JSON differs from one saved: %v", diff)
	}
}

func TestNewGame_WithLoadGameObjectState(t *testing.T) {
	orig := NewGame()
	defaultGameBytes := gamefile.DefaultGameJSON

	loaded, err := LoadGame(bytes.NewReader(defaultGameBytes))
	if err != nil {
		t.Fatalf("Unable to load game due to %s\n", err)
	}
	diff := deep.Equal(orig, loaded)
	if diff != nil {
		t.Errorf("Original game object differs from one loaded from JSON: %v", diff)
	}
}

func TestNewGame_WithLoadGameBadData(t *testing.T) {
	gameBytes := "{\"bad\":\"data\"}"

	_, err := LoadGame(bytes.NewReader([]byte(gameBytes)))
	if err == nil {
		t.Fatalf("Expected an error when loading bad JSON data %s\n", err)
	}
}
