package engine

import (
	"errors"
	"golang.org/x/exp/slices"
	"reflect"
	"strings"
	"testing"
)

func TestParseGiveInvalidCases(t *testing.T) {
	// test cases
	tests := []struct {
		name         string
		game         *Game
		words        []string
		err          error
		expected     ResponseContent
		expectedDesc string
	}{
		{
			name:         "No words",
			game:         &Game{},
			words:        []string{},
			err:          nil,
			expected:     ResponseText,
			expectedDesc: "What do you want to give",
		},
		{
			name: "Invalid item",
			game: &Game{
				Status: &Status{LocationId: "LOC1"},
				Rooms:  map[string]*Room{"LOC1": {}},
			},
			words:        []string{"unknown", "to", "unrelated"},
			err:          nil,
			expected:     ResponseText,
			expectedDesc: "Sorry, I can't see a unknown",
		},
		{
			name: "Invalid conjunction",
			game: &Game{
				Status: &Status{UserObjects: []Object{{Name: "Unknown"}}},
			},
			words:        []string{"unknown", "banana", "unrelated"},
			err:          nil,
			expected:     ResponseText,
			expectedDesc: "what do you want to use with what",
		},
		{
			name: "Invalid person",
			game: &Game{
				Status: &Status{LocationId: "LOC1", UserObjects: []Object{{Name: "Unknown"}}},
				Rooms:  map[string]*Room{"LOC1": {}},
			},
			words:        []string{"unknown", "to", "unrelated"},
			err:          nil,
			expected:     ResponseText,
			expectedDesc: "I can't see a person called 'unrelated'",
		},
		{
			name: "Valid person with no interaction",
			game: &Game{
				Status: &Status{LocationId: "LOC1", UserObjects: []Object{{Name: "Gift"}}},
				Rooms: map[string]*Room{"LOC1": {
					People: []*Person{
						{Name: "Alice"},
					},
				}},
			},
			words:        []string{"gift", "to", "Alice"},
			err:          nil,
			expected:     ResponseText,
			expectedDesc: "That doesn't seem to do anything",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			result, desc, _, err := parseWords(tc.game, "give "+strings.Join(tc.words, " "))
			if !reflect.DeepEqual(result, tc.expected) {
				t.Errorf("Test %s failed: expected %v, got %v", tc.name, tc.expected, result)
			}
			if !errors.Is(err, tc.err) {
				t.Errorf("Test %s failed: expected error %v, got %v", tc.name, tc.err, err)
			}
			if !strings.Contains(desc, tc.expectedDesc) {
				t.Errorf("Test %s failed: expected description %v, to contain %v", tc.name, tc.expectedDesc, desc)
			}
		})
	}
}

func TestParseGiveUserObjectReplacePerson(t *testing.T) {
	userObject := Object{ID: "GIFT", Name: "Gift"}
	person := Person{
		Name: "Alice",
		Interactions: []PersonInteraction{
			{
				ObjectID:        "GIFT",
				Type:            PersonInteractionGive,
				ReplaceOrigin:   true,
				ReplacePerson:   true,
				Description:     "New Description",
				CloseDirections: []Direction{North},
				OpenDirections:  []direction{{South, "LOC3"}},
				Success:         true,
				NewRoomDesc:     "New Room Description",
				ReceiveObjects:  []Object{{ID: "RECEIPT", Name: "Receipt"}},
			},
		},
	}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{userObject}},
		Rooms: map[string]*Room{"LOC1": {
			Description: "Original description",
			Locations:   map[Direction]string{North: "LOC2"},
			People:      []*Person{&person},
		}},
	}

	result, desc, _, _ := parseWords(game, "give gift to alice")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if !strings.Contains(desc, person.Description) {
		t.Errorf("Expected description %v, to contain %v", desc, person.Description)
	}
	if slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return userObject.ID == obj.ID }) {
		t.Errorf("Expected object to have been removed from user: %v", userObject)
	}
	if !slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return "RECEIPT" == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to user: %v", userObject)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].People, func(person *Person) bool { return person.Name == "Alice" }) {
		t.Errorf("Expected person to have left the room: %v", person)
	}
	if game.Rooms["LOC1"].Description != person.Interactions[0].NewRoomDesc {
		t.Errorf("Expected new room description, got %v expected %v", game.Rooms["LOC1"].Description, person.Interactions[0].NewRoomDesc)
	}
	_, ok := game.Rooms["LOC1"].Locations[North]
	if ok {
		t.Errorf("Expected North location to be closed, room locations %v", game.Rooms["LOC1"].Locations)
	}
	newLoc, ok := game.Rooms["LOC1"].Locations[South]
	if !ok || newLoc != "LOC3" {
		t.Errorf("Expected South location to be opened to LOC3, room locations %v", game.Rooms["LOC1"].Locations)
	}
}

func TestParseGiveRoomObject(t *testing.T) {
	roomObject := Object{ID: "GIFT", Name: "Gift"}
	person := Person{
		Name: "Alice",
		Interactions: []PersonInteraction{
			{
				ObjectID:       "GIFT",
				Type:           PersonInteractionGive,
				ReplaceOrigin:  true,
				Success:        true,
				ReceiveObjects: []Object{{ID: "RECEIPT", Name: "Receipt"}},
			},
		},
	}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{}},
		Rooms: map[string]*Room{"LOC1": {
			Description: "Original description",
			Locations:   map[Direction]string{North: "LOC2"},
			People:      []*Person{&person},
			Objects:     []*Object{&roomObject},
		}},
	}

	result, _, _, _ := parseWords(game, "give gift to alice")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return roomObject.ID == obj.ID }) {
		t.Errorf("Expected object to have been removed from room: %v", roomObject)
	}
	if !slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return "RECEIPT" == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to user: %v", roomObject)
	}
}

func TestParseGiveRoomMutationObject(t *testing.T) {
	roomObject := Object{ID: "GIFT", Name: "Gift"}
	person := Person{
		Name: "Alice",
		Interactions: []PersonInteraction{
			{
				ObjectID:       "GIFT",
				Type:           PersonInteractionGive,
				ReplaceOrigin:  true,
				Success:        true,
				ReceiveObjects: []Object{{ID: "RECEIPT", Name: "Receipt"}},
			},
		},
	}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{{ID: "PASS"}}},
		Rooms: map[string]*Room{"LOC1": {
			Description: "Original description",
			Locations:   map[Direction]string{North: "LOC2"},
			People:      []*Person{&person},
			Objects:     []*Object{},
			Mutations: []*RoomMutation{
				{
					ObjectID: "PASS",
					Objects:  []*Object{&roomObject},
				},
			},
		}},
	}

	result, _, _, _ := parseWords(game, "give gift to alice")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].Mutations[0].Objects, func(obj *Object) bool { return roomObject.ID == obj.ID }) {
		t.Errorf("Expected object to have been removed from mutation room: %v", roomObject)
	}
	if !slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return "RECEIPT" == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to user: %v", roomObject)
	}
}
