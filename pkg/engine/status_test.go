package engine

import (
	"reflect"
	"testing"
)

func TestStatus_GetSelectedWeapon(t *testing.T) {
	sword := Object{ID: "SWORD", Name: "Sword"}
	cheese := Object{ID: "CHEESE", Name: "Cheese"}
	book := Object{ID: "BOOK", Name: "Book"}
	tests := []struct {
		name             string
		userObjects      []Object
		selectedWeaponID *string
		expected         *Object
	}{
		{
			name:             "User wields carried weapon",
			userObjects:      []Object{cheese, sword, book},
			selectedWeaponID: &sword.ID,
			expected:         &sword,
		}, {
			name:             "User wields un-carried weapon",
			userObjects:      []Object{cheese, book},
			selectedWeaponID: &sword.ID,
			expected:         nil,
		}, {
			name:             "User wields no weapon",
			userObjects:      []Object{cheese, sword, book},
			selectedWeaponID: nil,
			expected:         nil,
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			status := Status{
				UserObjects:      testCase.userObjects,
				SelectedWeaponID: testCase.selectedWeaponID,
			}
			result := status.getSelectedWeapon()
			if reflect.DeepEqual(result, testCase.expected) == false {
				t.Errorf("Status.getSelectedWeapon() = %v, want %v", result, testCase.expected)
			}
		})
	}
}
