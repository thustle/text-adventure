package engine

import (
	"errors"
	"fmt"
	"golang.org/x/exp/slices"
	"math/rand/v2"
	"strings"
)

type ResponseContent int

const (
	ResponseEmpty ResponseContent = iota
	ResponseText
)

var textToDirection = map[string]Direction{
	"north": North,
	"n":     North,
	"south": South,
	"s":     South,
	"east":  East,
	"e":     East,
	"west":  West,
	"w":     West,
}

func parseCommand(g *Game, cmd string) (ResponseContent, string, error) {
	if g.Status.Health == 0 {
		return ResponseText, "You are dead. Game over.", nil
	}
	if g.Status.Won {
		return ResponseText, "You won. Game over.", nil
	}
	response, text, canBeAttacked, err := parseWords(g, cmd)
	if canBeAttacked {
		_, attackText, wasAttacked := receiveAttack(g)
		if wasAttacked {
			text = text + fmt.Sprintf(" %s", attackText)
		}
	}
	if g.Status.Health == 0 {
		text = text + "\nGame over."
	}
	if g.Status.Won {
		text = text + "\nYou have successfully completed your quest. Game over."
	}
	return response, text, err
}

func parseWords(g *Game, cmd string) (ResponseContent, string, bool, error) {
	words := strings.Split(strings.Trim(cmd, " "), " ")
	response := ResponseText
	text := notKnown
	canBeAttacked := false
	var err error
	switch strings.ToLower(words[0]) {
	case "help":
		text = help
	case "look":
		response, text, err = parseLook(g, words[1:])
	case "examine":
		response, text, err = parseLookAt(g, words[1:])
	case "go", "move":
		response, text, err = parseMove(g, words[1:])
		if g.Rooms[g.Status.LocationId].WinGame {
			g.Status.Won = true
		}
	case "attack", "hit":
		response, text, err = parseAttack(g, words[1:])
		canBeAttacked = true
	case "get":
		response, text, err = parseGet(g, words[1:])
		canBeAttacked = true
	case "pick":
		response, text, err = parsePickUp(g, words[1:])
		canBeAttacked = true
	case "drop":
		response, text, err = parseDrop(g, words[1:])
		canBeAttacked = true
	case "wield":
		response, text, err = parseWield(g, words[1:])
		canBeAttacked = true
	case "eat", "drink", "consume":
		response, text, err = parseConsume(g, words[1:])
		canBeAttacked = true
	case "use":
		response, text, err = parseUse(g, words[1:])
		canBeAttacked = true
	case "talk":
		response, text, err = parseTalk(g, words[1:])
		canBeAttacked = true
	case "give", "offer":
		response, text, err = parseGive(g, words[1:])
		canBeAttacked = true
	}
	return response, text, canBeAttacked, err
}

func parseLook(g *Game, words []string) (ResponseContent, string, error) {
	room := g.Rooms[g.Status.LocationId]
	if len(words) == 0 {
		return ResponseEmpty, room.DescriptionForUser(g.Status), nil
	} else if words[0] == "at" {
		return parseLookAt(g, words[1:])
	} else {
		return ResponseText, "Sorry, I don't understand. Look what?", nil
	}
}

func parseLookAt(g *Game, words []string) (ResponseContent, string, error) {
	room := g.Rooms[g.Status.LocationId]
	if len(words) > 0 {
		itemName := strings.ToLower(strings.Join(words, " "))
		for _, obj := range room.Objects {
			if strings.ToLower(obj.Name) == itemName {
				return ResponseText, obj.Description, nil
			}
		}
		mutObject, _, _ := getMutationObject(g, room, itemName)
		if mutObject != nil {
			return ResponseText, mutObject.Description, nil
		}
		for _, obj := range g.Status.UserObjects {
			if strings.ToLower(obj.Name) == itemName {
				return ResponseText, obj.Description, nil
			}
		}
		for _, obj := range room.People {
			if strings.ToLower(obj.Name) == itemName {
				return ResponseText, obj.Description, nil
			}
		}
		for _, obj := range room.Enemies {
			if strings.ToLower(obj.Name) == itemName {
				return ResponseText, obj.Description, nil
			}
		}
		return ResponseText, "I don't see that here", nil
	}
	return ResponseText, "What do you want to look at?", nil
}

func parseMove(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		dirText := words[0]
		room := g.Rooms[g.Status.LocationId]
		roomDirections := room.LocationsForUser(g.Status)
		dir, ok := textToDirection[dirText]
		if !ok {
			return ResponseText, "I don't understand that direction.", nil
		}
		if roomDirections[dir] != "" {
			g.Status.LocationId = roomDirections[dir]
			return ResponseEmpty, g.Rooms[g.Status.LocationId].Description, nil
		} else {
			return ResponseText, "You can't go that way.", nil
		}
	}
	return ResponseText, "Please say a direction.", nil
}

func parseAttack(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		room := g.Rooms[g.Status.LocationId]
		enemyName := strings.ToLower(strings.Join(words, " "))
		for i, enemy := range room.Enemies {
			if strings.ToLower(enemy.Name) == enemyName {
				txt, desc, err := attackEnemy(g, enemy)
				if enemy.Health <= 0 {
					desc = desc + "\n" + enemy.DeathDescription
					room.Objects = append(room.Objects, enemy.ItemsWhenDead...)
					newEnemiesList := slices.Delete(room.Enemies, i, i+1)
					room.Enemies = newEnemiesList
				}
				return txt, desc, err
			}
		}
		return ResponseText, "That enemy isn't here.", nil
	}
	return ResponseText, "Who?", nil
}

func attackEnemy(g *Game, enemy *Enemy) (ResponseContent, string, error) {
	selectedWeapon := g.Status.getSelectedWeapon()
	if selectedWeapon == nil {
		damageInflicted := enemy.ReceiveDamage(2)
		return ResponseText, fmt.Sprintf("You hit out with your fists and inflict %d damage.", damageInflicted), nil
	} else {
		damageInflicted := enemy.ReceiveDamage(selectedWeapon.AttackDamage)
		return ResponseText, fmt.Sprintf("You hit out with your %s and inflict %d damage.", selectedWeapon.Name, damageInflicted), nil
	}
}

func receiveAttack(g *Game) (ResponseContent, string, bool) {
	room := g.Rooms[g.Status.LocationId]
	if len(room.Enemies) > 0 {
		enemy := room.Enemies[0]
		enemyAttackDamage := enemy.GetAttackDamage()
		attackDesc := fmt.Sprintf("\n%s. You take %d damage.", enemy.AttackDescription, enemyAttackDamage)
		newUserHealth := int(g.Status.Health) - int(enemyAttackDamage)
		if newUserHealth <= 0 {
			g.Status.Health = 0
			return ResponseText, attackDesc + " You have died.", true
		} else {
			g.Status.Health = Health(newUserHealth)
			return ResponseText, attackDesc, true
		}
	}
	return ResponseText, "", false
}

func parsePickUp(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) > 1 && words[0] == "up" {
		return parseGet(g, words[1:])
	}
	return ResponseText, "What?", nil
}

func parseGet(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		room := g.Rooms[g.Status.LocationId]
		itemName := strings.ToLower(strings.Join(words, " "))
		for i, obj := range room.Objects {
			if strings.ToLower(obj.Name) == itemName {
				if !obj.Obtainable {
					return ResponseText, "You can't pick that up!", nil
				}
				newObjectList := slices.Delete(room.Objects, i, i+1)
				room.Objects = newObjectList
				g.Status.UserObjects = append(g.Status.UserObjects, *obj)
				if obj.WinGame {
					g.Status.Won = true
				}
				return ResponseText, fmt.Sprintf("You pick up the %s.", itemName), nil
			}
		}
		mutObject, mutObjIndex, mutation := getMutationObject(g, room, itemName)
		if mutObject != nil {
			if !mutObject.Obtainable {
				return ResponseText, "You can't pick that up!", nil
			}
			newObjectList := slices.Delete(mutation.Objects, mutObjIndex, mutObjIndex+1)
			mutation.Objects = newObjectList
			g.Status.UserObjects = append(g.Status.UserObjects, *mutObject)
			if mutObject.WinGame {
				g.Status.Won = true
			}
			return ResponseText, fmt.Sprintf("You pick up the %s.", itemName), nil
		}
		return ResponseText, "I can't see that here.", nil
	}
	return ResponseText, "What?", nil
}

func getMutationObject(g *Game, room *Room, itemName string) (*Object, int, *RoomMutation) {
	mutations := room.mutationsForUser(g.Status)
	for _, mutation := range mutations {
		for i, obj := range mutation.Objects {
			if strings.ToLower(obj.Name) == itemName {
				return obj, i, mutation
			}
		}
	}
	return nil, 0, nil
}

func parseDrop(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		room := g.Rooms[g.Status.LocationId]
		itemName := strings.ToLower(strings.Join(words, " "))
		for i, obj := range g.Status.UserObjects {
			if strings.ToLower(obj.Name) == itemName {
				newObjectList := slices.Delete(g.Status.UserObjects, i, i+1)
				g.Status.UserObjects = newObjectList
				room.Objects = append(room.Objects, &obj)
				return ResponseText, fmt.Sprintf("You drop the %s", itemName), nil
			}
		}
		return ResponseText, "You don't have that item.", nil
	}
	return ResponseText, "What?", nil
}

func parseWield(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		itemName := strings.ToLower(strings.Join(words, " "))
		for _, obj := range g.Status.UserObjects {
			if strings.ToLower(obj.Name) == itemName {
				if obj.AttackDamage == 0 {
					return ResponseText, "That item is not a weapon.", nil
				}
				g.Status.SelectedWeaponID = &obj.ID
				return ResponseText, fmt.Sprintf("You wield the %s", itemName), nil
			}
		}
		return ResponseText, "You don't have that item.", nil
	}
	return ResponseText, "What?", nil
}

func parseConsume(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		itemName := strings.ToLower(strings.Join(words, " "))
		for i, obj := range g.Status.UserObjects {
			if strings.ToLower(obj.Name) == itemName {
				if !obj.Consumable {
					return ResponseText, "Yuck, no way!", nil
				}
				change := int(g.Status.Health) + obj.HealthBenefit
				if change < 0 {
					change = 0
				} else if change > 100 {
					change = 100
				}
				g.Status.Health = Health(change)
				newObjectList := slices.Delete(g.Status.UserObjects, i, i+1)
				g.Status.UserObjects = newObjectList
				if obj.ConsumeDesc != "" {
					return ResponseText, obj.ConsumeDesc, nil
				} else {
					return ResponseText, fmt.Sprintf("You consume the %s", itemName), nil
				}
			}
		}
		return ResponseText, "You don't have that item to consume.", nil
	}
	return ResponseText, "What?", nil
}

func parseUse(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		room := g.Rooms[g.Status.LocationId]
		wordSeparator := findUseConjunctionIndex(words, "on", "with")
		if wordSeparator <= 0 || wordSeparator >= len(words)-1 {
			return ResponseText, "Sorry, what do you want to use with what?", nil
		}
		originItemName := strings.ToLower(strings.Join(words[:wordSeparator], " "))
		actionItemName := strings.ToLower(strings.Join(words[wordSeparator+1:], " "))
		originItem, originItemPos, originCarried, originMutation, err := findObjectItemByName(g, originItemName)
		if err != nil {
			return ResponseText, fmt.Sprintf("Sorry, I can't see a %s", originItemName), nil
		}
		actionItem, actionItemPos, actionCarried, actionMutation, err := findObjectItemByName(g, actionItemName)
		if err != nil {
			return ResponseText, fmt.Sprintf("Sorry, I can't see a %s", actionItemName), nil
		}
		for _, interaction := range originItem.Interactions {
			if interaction.Type == ObjectInteractionUse && interaction.ObjectID == actionItem.ID {
				if interaction.Success {
					if interaction.ReplaceObject {
						if actionCarried {
							newObjectList := slices.Delete(g.Status.UserObjects, actionItemPos, actionItemPos+1)
							g.Status.UserObjects = newObjectList
						} else {
							if actionMutation == nil {
								newObjectList := slices.Delete(room.Objects, actionItemPos, actionItemPos+1)
								room.Objects = newObjectList
							} else {
								newObjectList := slices.Delete(actionMutation.Objects, actionItemPos, actionItemPos+1)
								actionMutation.Objects = newObjectList
							}
						}
					}
					if interaction.ReplaceOrigin {
						if originCarried {
							newObjectList := slices.Delete(g.Status.UserObjects, originItemPos, originItemPos+1)
							g.Status.UserObjects = newObjectList
						} else {
							if originMutation == nil {
								newObjectList := slices.Delete(room.Objects, originItemPos, originItemPos+1)
								room.Objects = newObjectList
							} else {
								newObjectList := slices.Delete(originMutation.Objects, originItemPos, originItemPos+1)
								originMutation.Objects = newObjectList
							}
						}
					}
					for _, obj := range interaction.ProducesObjects {
						if actionCarried && obj.Obtainable {
							g.Status.UserObjects = append(g.Status.UserObjects, obj)
						} else {
							if actionMutation == nil {
								room.Objects = append(room.Objects, &obj)
							} else {
								actionMutation.Objects = append(actionMutation.Objects, &obj)
							}
						}
					}
					if interaction.WinGame {
						g.Status.Won = true
					}
					return ResponseText, interaction.Description, nil
				} else {
					return ResponseText, interaction.Description, nil
				}
			}
		}
		return ResponseText, "That doesn't seem to do anything.", nil
	}
	return ResponseText, "Use what?", nil
}

func findUseConjunctionIndex(words []string, conjunctions ...string) int {
	return slices.IndexFunc(words, func(s string) bool {
		return slices.Contains(conjunctions, s)
	})
}

func findObjectItemByName(g *Game, itemName string) (*Object, int, bool, *RoomMutation, error) {
	for i, obj := range g.Status.UserObjects {
		if strings.ToLower(obj.Name) == itemName {
			return &obj, i, true, nil, nil
		}
	}
	room := g.Rooms[g.Status.LocationId]
	for i, obj := range room.Objects {
		if strings.ToLower(obj.Name) == itemName {
			return obj, i, false, nil, nil
		}
	}
	mutObject, mutObjIndex, mutation := getMutationObject(g, room, itemName)
	if mutObject != nil {
		return mutObject, mutObjIndex, false, mutation, nil
	}
	return nil, 0, false, nil, errors.New("object not found")
}

func findPersonByName(g *Game, personName string) (*Person, int, error) {
	room := g.Rooms[g.Status.LocationId]
	for i, person := range room.People {
		if strings.ToLower(person.Name) == personName {
			return person, i, nil
		}
	}
	return nil, 0, errors.New("person not found")
}

func parseTalk(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 2 && words[0] == "to" {
		room := g.Rooms[g.Status.LocationId]
		personName := strings.ToLower(strings.Join(words[1:], " "))
		for _, person := range room.People {
			if strings.ToLower(person.Name) == personName {
				if len(person.TalkLines) == 0 {
					return ResponseText, "You get no answer.", nil
				}
				lineIndex := getRandomIndex(len(person.TalkLines))
				return ResponseText, person.TalkLines[lineIndex], nil
			}
		}
		for _, enemy := range room.Enemies {
			if strings.ToLower(enemy.Name) == personName {
				if len(enemy.TalkLines) == 0 {
					return ResponseText, "You get no answer.", nil
				}
				lineIndex := getRandomIndex(len(enemy.TalkLines))
				return ResponseText, enemy.TalkLines[lineIndex], nil
			}
		}
		return ResponseText, "I can't see that person.", nil
	}
	return ResponseText, "\"Hello\", you shout, but no one is listening.", nil
}

func getRandomIndex(length int) int {
	return int(rand.Float64() * float64(length))
}

func parseGive(g *Game, words []string) (ResponseContent, string, error) {
	if len(words) >= 1 {
		room := g.Rooms[g.Status.LocationId]
		wordSeparator := findUseConjunctionIndex(words, "to")
		if wordSeparator <= 0 || wordSeparator >= len(words)-1 {
			return ResponseText, "Sorry, what do you want to use with what?", nil
		}
		originItemName := strings.ToLower(strings.Join(words[:wordSeparator], " "))
		personName := strings.ToLower(strings.Join(words[wordSeparator+1:], " "))
		originItem, originItemPos, originCarried, originMutation, err := findObjectItemByName(g, originItemName)
		if err != nil {
			return ResponseText, fmt.Sprintf("Sorry, I can't see a %s", originItemName), nil
		}
		person, personPos, err := findPersonByName(g, personName)
		if err != nil {
			return ResponseText, fmt.Sprintf("Sorry, I can't see a person called '%s'", personName), nil
		}
		for _, interaction := range person.Interactions {
			if interaction.Type == PersonInteractionGive && interaction.ObjectID == originItem.ID {
				if interaction.Success {
					if interaction.ReplaceOrigin {
						if originCarried {
							newObjectList := slices.Delete(g.Status.UserObjects, originItemPos, originItemPos+1)
							g.Status.UserObjects = newObjectList
						} else {
							if originMutation == nil {
								newObjectList := slices.Delete(room.Objects, originItemPos, originItemPos+1)
								room.Objects = newObjectList
							} else {
								newObjectList := slices.Delete(originMutation.Objects, originItemPos, originItemPos+1)
								originMutation.Objects = newObjectList
							}
						}
					}
					if interaction.ReplacePerson {
						newPersonList := slices.Delete(room.People, personPos, personPos+1)
						room.People = newPersonList
					}
					for _, obj := range interaction.ReceiveObjects {
						g.Status.UserObjects = append(g.Status.UserObjects, obj)
					}
					for _, dir := range interaction.CloseDirections {
						delete(room.Locations, dir)
					}
					for _, dir := range interaction.OpenDirections {
						room.Locations[dir.DestinationDirection] = dir.ToID
					}
					room.Description = interaction.NewRoomDesc
					person.TalkLines = interaction.NewLines
				}
				return ResponseText, interaction.Description, nil
			}
		}
		return ResponseText, "That doesn't seem to do anything.", nil
	}
	return ResponseText, "What do you want to give and to whom?", nil
}
