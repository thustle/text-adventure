package engine

type Health uint

type Direction uint

const (
	North Direction = iota
	South
	East
	West
)

type Status struct {
	Health           Health   `json:"health"`
	UserObjects      []Object `json:"objects"`
	LocationId       string   `json:"locationId"`
	SelectedWeaponID *string  `json:"selectedWeaponId"`
	Won              bool     `json:"won"`
}

func (s *Status) getSelectedWeapon() *Object {
	if s.SelectedWeaponID == nil {
		return nil
	}
	for _, obj := range s.UserObjects {
		if *s.SelectedWeaponID == obj.ID {
			return &obj
		}
	}
	return nil
}
