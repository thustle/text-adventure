package engine

type ObjectInteractionType int

const (
	ObjectInteractionUse ObjectInteractionType = iota
)

type ObjectInteraction struct {
	ObjectID        string                `json:"objectId"`
	Type            ObjectInteractionType `json:"type"`
	Description     string                `json:"description"`
	Success         bool                  `json:"success"`
	ReplaceObject   bool                  `json:"replaceObject"`
	ReplaceOrigin   bool                  `json:"replaceOrigin"`
	ProducesObjects []Object              `json:"producesObjects"`
	WinGame         bool                  `json:"won"`
}

type Object struct {
	ID            string              `json:"id"`
	Name          string              `json:"name"`
	Description   string              `json:"description"`
	Obtainable    bool                `json:"obtainable"`
	AttackDamage  uint                `json:"attackDamage"`
	IsWeapon      bool                `json:"weapon"`
	Consumable    bool                `json:"consumable"`
	HealthBenefit int                 `json:"healthBenefit"`
	ConsumeDesc   string              `json:"consumeDescription"`
	Interactions  []ObjectInteraction `json:"interactions"`
	WinGame       bool                `json:"won"`
}
