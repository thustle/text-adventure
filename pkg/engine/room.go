package engine

type RoomMutation struct {
	ObjectID    string               `json:"objectId"`
	Description string               `json:"description"`
	Locations   map[Direction]string `json:"Locations"`
	Objects     []*Object            `json:"objects"`
}

type Room struct {
	ID          string               `json:"id"`
	Description string               `json:"description"`
	Locations   map[Direction]string `json:"locations"`
	Mutations   []*RoomMutation      `json:"mutations"`
	Objects     []*Object            `json:"objects"`
	Enemies     []*Enemy             `json:"enemies"`
	People      []*Person            `json:"people"`
	WinGame     bool                 `json:"won"`
}

func (r *Room) LocationsForUser(status *Status) map[Direction]string {
	mutations := r.mutationsForUser(status)
	if len(mutations) > 0 {
		return mutations[0].Locations
	}
	return r.Locations
}

func (r *Room) DescriptionForUser(status *Status) string {
	mutations := r.mutationsForUser(status)
	if len(mutations) > 0 {
		return mutations[0].Description
	}
	return r.Description
}

func (r *Room) ObjectsForUser(status *Status) []*Object {
	mutations := r.mutationsForUser(status)
	objects := make([]*Object, 0)
	for _, o := range r.Objects {
		objects = append(objects, o)
	}
	for _, m := range mutations {
		for _, mo := range m.Objects {
			objects = append(objects, mo)
		}
	}
	return objects
}

func (r *Room) mutationsForUser(status *Status) []*RoomMutation {
	userMutations := make([]*RoomMutation, 0)
	for _, m := range r.Mutations {
		for _, o := range status.UserObjects {
			if o.ID == m.ObjectID {
				userMutations = append(userMutations, m)
			}
		}
	}
	return userMutations
}

type direction struct {
	DestinationDirection Direction `json:"direction"`
	ToID                 string    `json:"toId"`
}
