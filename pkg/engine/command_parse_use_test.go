package engine

import (
	"golang.org/x/exp/slices"
	"strings"
	"testing"
)

func TestParseUseInvalidScenarios(t *testing.T) {
	tt := []struct {
		name     string
		g        *Game
		words    []string
		expected ResponseContent
		msg      string
	}{
		{
			name:     "No words",
			g:        &Game{},
			words:    []string{},
			expected: ResponseText,
			msg:      "Use what?",
		},
		{
			name: "use single object",
			g: &Game{
				Status: &Status{
					LocationId: "1",
				},
			},
			words:    []string{"object1"},
			expected: ResponseText,
			msg:      "Sorry, what do you want to use with what?",
		},
		{
			name: "Missing object1",
			g: &Game{
				Status: &Status{
					LocationId: "1",
				},
				Rooms: map[string]*Room{
					"1": {
						ID: "1",
					},
				},
			},
			words:    []string{"object1", "with", "object2"},
			expected: ResponseText,
			msg:      "Sorry, I can't see a object1",
		},
		{
			name: "Missing object2",
			g: &Game{
				Status: &Status{
					LocationId:  "1",
					UserObjects: []Object{{Name: "object1"}},
				},
				Rooms: map[string]*Room{
					"1": {
						ID: "1",
					},
				},
			},
			words:    []string{"object1", "with", "object2"},
			expected: ResponseText,
			msg:      "Sorry, I can't see a object2",
		},
		{
			name: "Interaction doesn't exist",
			g: &Game{
				Status: &Status{
					LocationId:  "1",
					UserObjects: []Object{{Name: "object1"}},
				},
				Rooms: map[string]*Room{
					"1": {
						ID:      "1",
						Objects: []*Object{{Name: "object2"}},
					},
				},
			},
			words:    []string{"object1", "with", "object2"},
			expected: ResponseText,
			msg:      "That doesn't seem to do anything.",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			res, msg, _, _ := parseWords(tc.g, "use "+strings.Join(tc.words, " "))
			if tc.expected != res {
				t.Errorf("parseUse() = %v, want %v", res, tc.expected)
			}
			if !strings.Contains(msg, tc.msg) {
				t.Errorf("parseUse() = `%s`, want to contain `%s`", msg, tc.msg)
			}
		})
	}
}

func TestParseUseObjectWithAnotherReplaceUserObjects(t *testing.T) {
	ojInteraction := ObjectInteraction{
		ObjectID:      "VODKA",
		Type:          ObjectInteractionUse,
		Description:   "You mix up a screwdriver",
		ReplaceOrigin: true,
		ReplaceObject: true,
		Success:       true,
		ProducesObjects: []Object{
			{ID: "SCREWDRIVER", Name: "Screwdriver", Obtainable: true},
		},
	}
	matchObject := Object{ID: "OJ", Name: "Orange Juice",
		Interactions: []ObjectInteraction{ojInteraction},
	}
	strikerObject := Object{ID: "VODKA", Name: "Vodka"}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{matchObject, strikerObject}},
		Rooms:  map[string]*Room{"LOC1": {}},
	}

	result, desc, _, _ := parseWords(game, "use orange juice with vodka")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if !strings.Contains(desc, ojInteraction.Description) {
		t.Errorf("Expected description %v, to contain %v", desc, ojInteraction.Description)
	}
	if slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return matchObject.ID == obj.ID || strikerObject.ID == obj.ID }) {
		t.Errorf("Expected objects to have been removed from user: %v, %v", matchObject, strikerObject)
	}
	if !slices.ContainsFunc(game.Status.UserObjects, func(obj Object) bool { return ojInteraction.ProducesObjects[0].ID == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to user: %v", ojInteraction.ProducesObjects[0].ID)
	}
}

func TestParseUseObjectWithAnotherNoSuccess(t *testing.T) {
	ojInteraction := ObjectInteraction{
		ObjectID:    "VODKA",
		Type:        ObjectInteractionUse,
		Description: "You aren't allowed",
		Success:     false,
		ProducesObjects: []Object{
			{ID: "SCREWDRIVER", Name: "Screwdriver", Obtainable: true},
		},
	}
	matchObject := Object{ID: "OJ", Name: "Orange Juice",
		Interactions: []ObjectInteraction{ojInteraction},
	}
	strikerObject := Object{ID: "VODKA", Name: "Vodka"}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{}},
		Rooms: map[string]*Room{"LOC1": {
			Objects: []*Object{&matchObject, &strikerObject},
		}},
	}

	result, desc, _, _ := parseWords(game, "use orange juice with vodka")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if !strings.Contains(desc, ojInteraction.Description) {
		t.Errorf("Expected description %v, to contain %v", desc, ojInteraction.Description)
	}
	if !slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return matchObject.ID == obj.ID }) {
		t.Errorf("Expected objects to exist in room: %v", matchObject)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return ojInteraction.ProducesObjects[0].ID == obj.ID }) {
		t.Errorf("Expected receipt object to have not been given to room: %v", ojInteraction.ProducesObjects[0].ID)
	}
}

func TestParseUseObjectWithAnotherReplaceRoomObjects(t *testing.T) {
	ojInteraction := ObjectInteraction{
		ObjectID:      "VODKA",
		Type:          ObjectInteractionUse,
		Description:   "You mix up a screwdriver",
		ReplaceOrigin: true,
		ReplaceObject: true,
		Success:       true,
		ProducesObjects: []Object{
			{ID: "SCREWDRIVER", Name: "Screwdriver", Obtainable: true},
		},
		WinGame: true,
	}
	matchObject := Object{ID: "OJ", Name: "Orange Juice",
		Interactions: []ObjectInteraction{ojInteraction},
	}
	strikerObject := Object{ID: "VODKA", Name: "Vodka"}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{}},
		Rooms: map[string]*Room{"LOC1": {
			Objects: []*Object{&matchObject, &strikerObject},
		}},
	}

	result, desc, _, _ := parseWords(game, "use orange juice with vodka")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if !strings.Contains(desc, ojInteraction.Description) {
		t.Errorf("Expected description %v, to contain %v", desc, ojInteraction.Description)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return matchObject.ID == obj.ID || strikerObject.ID == obj.ID }) {
		t.Errorf("Expected objects to have been removed from room: %v, %v", matchObject, strikerObject)
	}
	if !slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return ojInteraction.ProducesObjects[0].ID == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to room: %v", ojInteraction.ProducesObjects[0].ID)
	}
	if !game.Status.Won {
		t.Errorf("Expected Won game to be set to true")
	}
}

func TestParseUseObjectWithAnotherReplaceMutationObjects(t *testing.T) {
	ojInteraction := ObjectInteraction{
		ObjectID:      "VODKA",
		Type:          ObjectInteractionUse,
		Description:   "You mix up a screwdriver",
		ReplaceOrigin: true,
		ReplaceObject: true,
		Success:       true,
		ProducesObjects: []Object{
			{ID: "SCREWDRIVER", Name: "Screwdriver", Obtainable: true},
		},
	}
	matchObject := Object{ID: "OJ", Name: "Orange Juice",
		Interactions: []ObjectInteraction{ojInteraction},
	}
	strikerObject := Object{ID: "VODKA", Name: "Vodka"}
	game := &Game{
		Status: &Status{LocationId: "LOC1", UserObjects: []Object{{ID: "BAR", Name: "BAR"}}},
		Rooms: map[string]*Room{"LOC1": {
			Objects: []*Object{&matchObject},
			Mutations: []*RoomMutation{
				{
					ObjectID: "BAR",
					Objects:  []*Object{&strikerObject},
				},
			},
		}},
	}

	result, desc, _, _ := parseWords(game, "use orange juice with vodka")
	if result != ResponseText {
		t.Errorf("Expected %v, got %v", ResponseText, result)
	}
	if !strings.Contains(desc, ojInteraction.Description) {
		t.Errorf("Expected description %v, to contain %v", desc, ojInteraction.Description)
	}
	if slices.ContainsFunc(game.Rooms["LOC1"].Objects, func(obj *Object) bool { return matchObject.ID == obj.ID || strikerObject.ID == obj.ID }) {
		t.Errorf("Expected objects to have been removed from room: %v, %v", matchObject, strikerObject)
	}
	if !slices.ContainsFunc(game.Rooms["LOC1"].Mutations[0].Objects, func(obj *Object) bool { return ojInteraction.ProducesObjects[0].ID == obj.ID }) {
		t.Errorf("Expected receipt object to have been given to mutation of room: %v", ojInteraction.ProducesObjects[0].ID)
	}
}
