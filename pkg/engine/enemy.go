package engine

import "math/rand/v2"

const (
	randMin = 0.7
	randMax = 1.3
)

type Enemy struct {
	ID                string    `json:"id"`
	Name              string    `json:"name"`
	Description       string    `json:"description"`
	RoomDescription   string    `json:"roomDescription"`
	Health            int       `json:"health"`
	Attack            uint      `json:"attack"`
	ItemsWhenDead     []*Object `json:"itemsWhenDead"`
	AttackDescription string    `json:"attackDescription"`
	DeathDescription  string    `json:"deathDescription"`
	TalkLines         []string  `json:"talkLines"`
}

func (e *Enemy) ReceiveDamage(attackStrength uint) int {
	damageMultiplier := randMin + rand.Float64()*(randMax-randMin)
	damageAmount := int(float64(attackStrength) * damageMultiplier)
	if e.Health < damageAmount {
		e.Health = 0
	} else {
		e.Health = e.Health - damageAmount
	}
	return damageAmount
}

func (e *Enemy) GetAttackDamage() uint {
	damageMultiplier := randMin + rand.Float64()*(randMax-randMin)
	return uint(float64(e.Attack) * damageMultiplier)
}
