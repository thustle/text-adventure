package engine

import (
	"errors"
	"fmt"
	"strings"
	"testing"
)

func TestParseLook(t *testing.T) {
	type testData struct {
		name            string
		game            *Game
		words           []string
		expectedContent ResponseContent
		expectedText    string
	}

	tests := []testData{
		{
			name: "Return I don't understand when words are incorrect",
			game: &Game{
				Status: &Status{
					LocationId: "TestLocation",
				},
				Rooms: map[string]*Room{
					"TestLocation": {
						Description: "Test Room",
					},
				},
			},
			words:           []string{"around"},
			expectedContent: ResponseText,
			expectedText:    "I don't understand",
		},
		{
			name: "Return I can't see that when not found",
			game: &Game{
				Status: &Status{
					LocationId: "TestLocation",
				},
				Rooms: map[string]*Room{
					"TestLocation": {
						Description: "Test Room",
					},
				},
			},
			words:           []string{"at", "clock"},
			expectedContent: ResponseText,
			expectedText:    "I don't see that here",
		},
		{
			name: "Return room description when no extra words given",
			game: &Game{
				Status: &Status{
					LocationId: "TestLocation",
				},
				Rooms: map[string]*Room{
					"TestLocation": {
						Description: "Test Room",
					},
				},
			},
			words:           []string{},
			expectedContent: ResponseEmpty,
			expectedText:    "Test Room",
		},
		{
			name: "Return mutation room description when no extra words given and mutation object held",
			game: &Game{
				Status: &Status{
					LocationId:  "TestLocation",
					UserObjects: []Object{{ID: "obj1"}},
				},
				Rooms: map[string]*Room{
					"TestLocation": {
						Description: "Test Room",
						Mutations: []*RoomMutation{
							{
								ObjectID:    "obj1",
								Description: "Mutation Room",
							},
						},
					},
				},
			},
			words:           []string{},
			expectedContent: ResponseEmpty,
			expectedText:    "Mutation Room",
		},
		{
			name: "Look for object when first word is 'at'",
			game: &Game{
				Status: &Status{
					LocationId: "TestLocation",
					UserObjects: []Object{
						{
							Name:        "object",
							Description: "object description",
						},
					},
				},
				Rooms: map[string]*Room{
					"TestLocation": {
						Description: "Test Room",
					},
				},
			},
			words:           []string{"at", "object"},
			expectedContent: ResponseText,
			expectedText:    "object description",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			responseContent, responseText, err := parseLook(tt.game, tt.words)
			if err != nil {
				t.Errorf("Unexpected error while parsing look: %v", err)
			}
			if tt.expectedContent != responseContent {
				t.Errorf("parseLook() Content = %v, got %v", tt.expectedContent, responseContent)
			}
			if !strings.Contains(responseText, tt.expectedText) {
				t.Errorf("parseLook() Text = %v, got %v", tt.expectedText, responseText)
			}
		})
	}
}

func TestParseLookAt(t *testing.T) {
	obj := &Object{ID: "obj1", Name: "obj1", Description: "object"}
	obj2 := &Object{ID: "obj2", Name: "obj2", Description: "object2"}
	person := &Person{Name: "dummy", Description: "person"}
	enemy := &Enemy{Name: "dummy", Description: "enemy"}

	tests := []struct {
		name     string
		game     *Game
		words    string
		wantResp ResponseContent
		wantDesc string
		wantErr  error
	}{
		{
			name: "no words",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms:  map[string]*Room{"home": {}},
			},
			words:    "look at",
			wantResp: ResponseText,
			wantDesc: "What do you want to look at?",
			wantErr:  nil,
		},
		{
			name: "look at object",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {Objects: []*Object{obj}},
				},
			},
			words:    fmt.Sprintf("look at %s", obj.Name),
			wantResp: ResponseText,
			wantDesc: obj.Description,
			wantErr:  nil,
		},
		{
			name: "examine object",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {Objects: []*Object{obj}},
				},
			},
			words:    fmt.Sprintf("examine %s", obj.Name),
			wantResp: ResponseText,
			wantDesc: obj.Description,
			wantErr:  nil,
		},
		{
			name: "look at mutation object",
			game: &Game{
				Status: &Status{
					LocationId:  "home",
					UserObjects: []Object{*obj},
				},
				Rooms: map[string]*Room{
					"home": {
						Mutations: []*RoomMutation{
							{
								ObjectID: obj.ID,
								Objects:  []*Object{obj2},
							},
						},
					},
				},
			},
			words:    fmt.Sprintf("look at %s", obj2.Name),
			wantResp: ResponseText,
			wantDesc: obj2.Description,
			wantErr:  nil,
		},
		{
			name: "look at person",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {People: []*Person{person}},
				},
			},
			words:    fmt.Sprintf("look at %s", person.Name),
			wantResp: ResponseText,
			wantDesc: person.Description,
			wantErr:  nil,
		},
		{
			name: "look at enemy",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {Enemies: []*Enemy{enemy}},
				},
			},
			words:    fmt.Sprintf("look at %s", enemy.Name),
			wantResp: ResponseText,
			wantDesc: enemy.Description,
			wantErr:  nil,
		},
		{
			name: "item not found",
			game: &Game{
				Status: &Status{LocationId: "home"},
				Rooms: map[string]*Room{
					"home": {},
				},
			},
			words:    "look at nonexistent",
			wantResp: ResponseText,
			wantDesc: "I don't see that here",
			wantErr:  nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResp, gotDesc, canBeAttacked, err := parseWords(tt.game, tt.words)
			if canBeAttacked {
				t.Errorf("parseLookAt() error, expected canBeAttacked == false")
				return
			}
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("parseLookAt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotResp != tt.wantResp {
				t.Errorf("parseLookAt() gotResp = %v, want %v", gotResp, tt.wantResp)
			}
			if gotDesc != tt.wantDesc {
				t.Errorf("parseLookAt() gotDesc = %v, want %v", gotDesc, tt.wantDesc)
			}
		})
	}
}
