package engine

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"text-adventure/pkg/gamefile"
)

type Game struct {
	GameName string           `json:"gameName"`
	Rooms    map[string]*Room `json:"rooms"`
	Status   *Status          `json:"status"`
}

func NewGame() Game {
	defaultGameBytes := gamefile.DefaultGameJSON
	game, _ := LoadGame(bytes.NewReader(defaultGameBytes))
	return game
}

func LoadGame(reader io.Reader) (Game, error) {
	game := Game{}
	decoder := json.NewDecoder(reader)
	err := decoder.Decode(&game)
	if game.Rooms == nil || game.Status == nil {
		err = errors.New("invalid Game data in JSON file")
	}
	return game, err
}

func SaveGame(writer io.Writer, game Game) error {
	decoder := json.NewEncoder(writer)
	err := decoder.Encode(game)
	return err
}

func (g *Game) ParseCommand(cmd string) (ResponseContent, string, error) {
	return parseCommand(g, cmd)
}
